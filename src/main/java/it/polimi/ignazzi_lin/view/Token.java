package it.polimi.ignazzi_lin.view;

import java.awt.Image;
import java.net.URL;

import javax.swing.ImageIcon;
 
public class Token extends ImageIcon {
    
    private String idPlayer;
    
    private String idShepherd;
    
    private String position;
    
    private JRoad jRoad;
    
    public Token(Image image) {
	super(image);
    }
    
    public Token(URL location) {
	super(location);
    }
   
    public Token(Image image, String idPlayer, String idSheperd, String position) {
	super(image);
	this.idPlayer = idPlayer;
	this.idShepherd = idSheperd;
	this.position = position;
	this.jRoad = null;
    }

    public Token(URL location, String idPlayer, String idSheperd, String position) {
	super(location);
	this.idPlayer = idPlayer;
	this.idShepherd = idSheperd;
	this.position = position;
	this.jRoad = null;
    }
    
    public String getIdPlayer() {
	return idPlayer;
    }

    public String getIdShepherd() {
	return idShepherd;
    }
    
    public String getPosition() {
	return position;
    }
    
    public void setPosition(String position) {
	this.position = position;
    }

    public JRoad getjRoad() {
        return jRoad;
    }

    public void setjRoad(JRoad jRoad) {
        this.jRoad = jRoad;
    }

    public boolean isShepherd() {
	return (idShepherd != null) ? true : false;
    }

}
