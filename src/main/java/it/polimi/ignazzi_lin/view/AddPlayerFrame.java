package it.polimi.ignazzi_lin.view;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class AddPlayerFrame extends JGameFrame {

    private JPanel mainPanel;
    
    private JPanel buttonsPanel;
    
    private JLabel messagePane;
    
    private JTextField textfield;
    
    private JButton addPlayer;
    
    private JButton startGame;
    
    public AddPlayerFrame(String title) throws HeadlessException {
	super(title);
	this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	mainPanel = new JPanel(new BorderLayout());
	buttonsPanel = new JPanel(new GridLayout(0, 1));
	messagePane = new JLabel();
	messagePane.setText("Inserire il nickname del giocatore 1:");
	
	Action sendAction = new AbstractAction("Aggiungi giocatore") {
	    int playerCount = 1;
	    @Override
	    public void actionPerformed(ActionEvent e) {
		playerCount++;
		textfield.setText("Aggiunto giocatore \"" + textfield.getText() + "\"!");
		textfield.requestFocus();
		textfield.selectAll();
		messagePane.setText("Inserire il nickname del giocatore " + playerCount + ":");
		if(playerCount > 4) {
		    messagePane.setText("Numero massimo raggiunto!");
		}
	    }
	};
	addPlayer = new JButton(sendAction);
	addPlayer.setName("addplayer");
	textfield = new JTextField();
	textfield.setName("addplayer");
	textfield.setText("Player 1");
	textfield.setAction(sendAction);
	startGame = new JButton("Inizia gioco");
	startGame.setName("startgameboard");
	startGame.setEnabled(false);
	
	buttonsPanel.add(addPlayer);
	buttonsPanel.add(startGame);
	mainPanel.add(messagePane, BorderLayout.NORTH);
	mainPanel.add(textfield, BorderLayout.CENTER);
	mainPanel.add(buttonsPanel, BorderLayout.SOUTH);
	
	this.add(mainPanel);
	this.pack();
	this.setLocationRelativeTo(null);
    }

    @Override
    public void addListener(ActionListener l) {
        this.addPlayer.addActionListener(l);
        this.textfield.addActionListener(l);
        this.startGame.addActionListener(l);
    }
    
    @Override
    public void manageButtons(int playersNumber) {
	if(playersNumber >= 1) {
	    this.startGame.setEnabled(true);
	}
	if(playersNumber >= 3) {
	    this.addPlayer.setEnabled(false);
	    this.textfield.setEnabled(false);
	}
    }
    
    public String getNickname() {
	return textfield.getText();
    }
    
}
