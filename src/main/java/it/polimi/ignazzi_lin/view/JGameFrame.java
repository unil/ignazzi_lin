package it.polimi.ignazzi_lin.view;

import java.awt.GraphicsConfiguration;
import java.awt.HeadlessException;
import java.awt.event.ActionListener;
import java.awt.event.MouseListener;

import javax.swing.JFrame;
import javax.swing.UIManager;

/**
 * Classe astratta rappresentante la finestra visibile all'utente.
 * 
 * @author unil
 *
 */
public abstract class JGameFrame extends JFrame {

    public JGameFrame() throws HeadlessException {
	super();
	
    }

    public JGameFrame(GraphicsConfiguration gc) {
	super(gc);
	
    }

    public JGameFrame(String title, GraphicsConfiguration gc) {
	super(title, gc);
	
    }

    public JGameFrame(String title) throws HeadlessException {
	super(title);
	try {
	    UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());;
	} catch(Exception e) {
	    
	}
    }
    
    public void addListener(ActionListener l) {}
    
    public void addListener(MouseListener l) {}
    
    public void manageButtons(int n) {};

}
