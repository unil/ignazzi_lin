package it.polimi.ignazzi_lin.view;

import it.polimi.ignazzi_lin.controller.NetAction;
import it.polimi.ignazzi_lin.controller.SheeplandAction;
import it.polimi.ignazzi_lin.controller.ViewAction;
import it.polimi.ignazzi_lin.model.AnimalType;
import it.polimi.ignazzi_lin.model.Model;
import it.polimi.ignazzi_lin.model.ModelNet;
import it.polimi.ignazzi_lin.model.TerrainType;

import java.awt.AlphaComposite;
import java.awt.Cursor;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Observable;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

/**
 * Classe che rappresenta la View nel modello MVC; in particolare implementa una
 * interfaccia utente grafica.
 * 
 * @author unil
 * 
 */
public class GUI extends View {

	private JGameFrame currentFrame;

	private StartingFrame startingFrame;

	private GameBoardFrame gameBoardFrame;

	private WaitingPlayerFrame waitingPlayerFrame;

	private MultiplayerConnectionFrame multiplayerConnectionFrame;

	private int playersNumber = 0;

	private int idCorrente;

	private String[] playersReady = new String[6];

	private String[] IDNickname = new String[11];
	private String[] playerOnline = new String[3];
	private String[] broadcastString = new String[2];
	private JButton button;

	public GUI() {
		for (int i = 0; i < 6; i++) {
			playersReady[i] = "0";
		}
		this.startingFrame = new StartingFrame("Sheepland");
		this.currentFrame = this.startingFrame;
		addPlayerActionListener(new SheeplandActionListener());
	}

	public static BufferedImage createResizedCopy(Image originalImage,
			final int scaledWidth, final int scaledHeight, boolean preserveAlpha) {
		int imageType = preserveAlpha ? BufferedImage.TYPE_INT_ARGB
				: BufferedImage.TYPE_INT_RGB;
		BufferedImage scaledBI = new BufferedImage(scaledWidth, scaledHeight,
				imageType);
		Graphics2D g = scaledBI.createGraphics();
		if (preserveAlpha) {
			g.setComposite(AlphaComposite.Src);
		}
		g.drawImage(originalImage, 0, 0, scaledWidth, scaledHeight, null);
		g.dispose();
		return scaledBI;
	}

	@Override
	public void run() {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				currentFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				currentFrame.setVisible(true);
				currentFrame.setResizable(false);
				currentFrame.setLocationRelativeTo(null);
			}
		});
	}

	@Override
	public void showJGameWindow(JGameFrame jGameWindow) {
		this.currentFrame.setVisible(false);
		this.currentFrame.dispose();
		this.currentFrame = jGameWindow;
		this.currentFrame.revalidate();
		this.currentFrame.repaint();
		this.run();
	}

	@Override
	public void addListenerCardsAnimals() {
		SheeplandMouseListener l = new SheeplandMouseListener();
		gameBoardFrame.addListenerToJCards(l);
		gameBoardFrame.addListenerToJAnimals(l);
	}

	@Override
	public void update(Observable o, Object arg) {
		if ((!(o instanceof Model)) && (!(o instanceof ModelNet))) {
			throw new IllegalArgumentException();
		} else {
			if (o instanceof Model) {
				showModel((Model) o, arg);
			}
		}
	}

	@Override
	protected void showModel(Model model, Object arg) {
		if (arg instanceof SheeplandAction) {
			SheeplandAction argcasted = (SheeplandAction) arg;
			SheeplandAction.Move move = argcasted.getMove();
			ArrayList<String> args = (ArrayList<String>) argcasted.getArgs();
			switch (move) {
			case MOVESHEPERD:
				String idPlayer = args.get(0);
				String idShepherd = args.get(1);
				String idRoad = args.get(2);
				int fences = Integer.parseInt(args.get(3));
				gameBoardFrame.moveJShepherd(idPlayer, idShepherd, idRoad,
						fences);
				break;
			case BUYCARD:
				TerrainType terrain = TerrainType.parseInput(args.get(0));
				String price = args.get(1);
				String count = args.get(2);
				gameBoardFrame.showBuyCard(terrain, price, count);
				gameBoardFrame.showMessage("Tessera \""
						+ terrain.toString().toLowerCase() + "\" acquisita!");
				break;
			case MOVEANIMAL:
				AnimalType animal = AnimalType.parseInput(args.get(0));
				String idOriginRegion = args.get(1);
				String idDestinationRegion = args.get(2);
				gameBoardFrame.moveJAnimal(animal, idOriginRegion,
						idDestinationRegion);
				break;
			case SPENDGOLD:
				String player = args.get(0);
				String gold = args.get(1);
				gameBoardFrame.playerSpendGold(player, gold);
				break;
			case ENDGAME:
				String message = args.get(0);
				showMessage(message);
				break;
			default:
				throw new IllegalArgumentException();
			}
		}

		if (arg instanceof ViewAction) {
			ViewAction argcasted = (ViewAction) arg;
			ViewAction.Command command = argcasted.getCommand();
			ArrayList<String> args = (ArrayList<String>) argcasted.getArgs();
			switch (command) {
			case SETPOSITION:
				String idPlayer = args.get(0);
				String idSheperd = args.get(1);
				String idRoad = args.get(2);
				String color = args.get(3).toLowerCase();
				gameBoardFrame.setTokenPosition(idPlayer, idSheperd, idRoad,
						color);
				break;
			default:
				throw new IllegalArgumentException();
			}
		}

		if (arg instanceof NetAction) {
			NetAction argcasted = (NetAction) arg;
			NetAction.Command command = argcasted.getCommand();
			ArrayList<String> args = (ArrayList<String>) argcasted.getArgs();
			switch (command) {
			case MESSAGE:
				String message = args.get(0);
				showMessage(message);
				break;
			case NEXTPLAYER:
				String currentShepherdID = args.get(0);
				String nextPlayerID = args.get(1);
				nextPlayer(currentShepherdID, nextPlayerID);
				break;
			case JROADNAME:
				String name = args.get(0);
				setJRoadsName(name);
				break;
			case CARDANIMALS:
				addListenerCardsAnimals();
				break;
			case SHOWGOLD:
				String playerGold = args.get(0);
				showGoldCounters(playerGold);
				break;
			default:
				throw new IllegalArgumentException();
			}
		}

		if (arg instanceof String[]) {
			String[] args = (String[]) arg;
			String command = (String) args[0];
			switch (command) {

			case "positionset":
				String idPlayer = args[1];
				String idSheperd = args[2];
				String idRoad = args[3];
				String color = args[4].toLowerCase();
				gameBoardFrame.setTokenPosition(idPlayer, idSheperd, idRoad,
						color);
				break;

			case "IDNickname":
				int size = Integer.parseInt(args[1]);
				IDNickname = new String[size];
				idCorrente = Integer.parseInt(args[2]);
				this.IDNickname = args;
				break;

			case "stringReady":
				playersReady = args;
				int c = 0;
				for (int i = 0; i < 6; i++) {
					if (playersReady[i].equals("v")) {
						c++;
					}
					playersNumber = c;
				}

				break;
			case "broadcastStart":
				broadcastString = args;
				if (playersReady[2].equals("v")) {
					waitingPlayerFrame.labelNotReady.setEnabled(false);
					waitingPlayerFrame.labelReady.setEnabled(true);
				}
				if (playersReady[3].equals("v")) {
					waitingPlayerFrame.labelNotReady1.setEnabled(false);
					waitingPlayerFrame.labelReady1.setEnabled(true);
				}
				if (playersReady[4].equals("v")) {
					waitingPlayerFrame.labelNotReady2.setEnabled(false);
					waitingPlayerFrame.labelReady2.setEnabled(true);
				}
				if (playersReady[5].equals("v")) {
					waitingPlayerFrame.labelNotReady3.setEnabled(false);
					waitingPlayerFrame.labelReady3.setEnabled(true);
				}
				waitingPlayerFrame.repaint();
				setChanged();
				notifyObservers("updateRequestPlayers");
				setChanged();
				notifyObservers("updateRequestPlayersReady");

				gameBoardFrame = new GameBoardFrame("Sheepland", playersNumber);
				JOptionPane
						.showMessageDialog(
								waitingPlayerFrame,
								("INIZIO DELLA PARTITA! - in bocca al lupo " + playerOnline[2]));
				showJGameWindow(gameBoardFrame);
				setChanged();
				notifyObservers(new ViewAction(ViewAction.Command.START));
				gameBoardFrame
						.addListenerToJRoads(new SheeplandMouseListener());
				break;

			default:
				throw new IllegalArgumentException();
			}
		}

		if (arg instanceof String) {

			int idRecieved = (Integer) arg;


		}
	}

	@Override
	public void showMessage(String message) {
		gameBoardFrame.showMessage(message);
	}

	@Override
	public void JOptionPane(String message) {
		JOptionPane.showMessageDialog(gameBoardFrame, message);
	}

	@Override
	public void showGoldCounters(String initialGold) {
		gameBoardFrame.showGoldCounters(initialGold);
	}

	@Override
	public void initializeJRoads(ArrayList<Integer> allX,
			ArrayList<Integer> allY, ArrayList<String> allIDs) {
		gameBoardFrame.addJRoadsToMap(allX, allY, allIDs);
	}

	@Override
	public void initializeJAnimals(ArrayList<Integer> allX,
			ArrayList<Integer> allY, ArrayList<String> allIDs) {
		gameBoardFrame.addJAnimals(allX, allY, allIDs);
	}

	@Override
	public void setJRoadsName(String name) {
		gameBoardFrame.setJRoadsName(name);
	}

	@Override
	public void nextPlayer(String currentPlayerID, String nextPlayerID) {
		gameBoardFrame.nextPlayer(currentPlayerID, nextPlayerID);
	}

	private void addPlayerActionListener(ActionListener l) {
		this.currentFrame.addListener(l);
	}

	class SheeplandActionListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			if (e.getSource() instanceof JButton) {
				button = (JButton) e.getSource();
				switch (button.getName()) {
				case "online":
					showJGameWindow(new MultiplayerFrame("Server o Client?"));
					addPlayerActionListener(new SheeplandActionListener());
					break;
				case "local":
					showJGameWindow(new AddPlayerFrame("Scegli i giocatori"));
					addPlayerActionListener(new SheeplandActionListener());
					break;
				case "server":
					setChanged();
					notifyObservers(new ViewAction(ViewAction.Command.RUNSERVER));
					break;
				case "client":
					multiplayerConnectionFrame = new MultiplayerConnectionFrame(
							"A quale server?", idCorrente);
					showJGameWindow(multiplayerConnectionFrame);
					addPlayerActionListener(new SheeplandActionListener());
					break;
				case "addplayer":
					addPlayer(button);
					break;
				case "startgameboard":
					gameBoardFrame = new GameBoardFrame("Sheepland",
							playersNumber);
					showJGameWindow(gameBoardFrame);
					setChanged();
					notifyObservers(new ViewAction(ViewAction.Command.START));
					gameBoardFrame
							.addListenerToJRoads(new SheeplandMouseListener());
					break;
				case "send":
					String ip = ((MultiplayerConnectionFrame) SwingUtilities
							.getWindowAncestor(button)).getIP();
					setChanged();

					notifyObservers(new ViewAction(	ViewAction.Command.RUNCLIENT, ip));

					playerOnline = multiplayerConnectionFrame
							.getPlayersOnline();
					// playerOnline[1]=Integer.toString(idCorrente);

					setChanged();
					notifyObservers(playerOnline);

					setChanged();
					notifyObservers("updateRequestPlayers");
					
					playersReady[0] = "stringReady";
					playersReady[1] = Integer.toString(idCorrente);
					waitingPlayerFrame = new WaitingPlayerFrame(
							"Lobby di attesa", playerOnline, IDNickname,
							playersReady);

					showJGameWindow(waitingPlayerFrame);
					addPlayerActionListener(new SheeplandActionListener());
					break;
				case "Pronto":
					
					playersReady[2] = "v";
					setChanged();
					notifyObservers(playersReady);
					setChanged();
					notifyObservers("updateRequestPlayers");
					setChanged();
					notifyObservers("updateRequestPlayersReady");
					if (Integer.parseInt(IDNickname[1]) > 5) {
						waitingPlayerFrame.lblNickname1.setText(IDNickname[6]);
					}
					if (Integer.parseInt(IDNickname[1]) > 7) {
						waitingPlayerFrame.lblNickname2.setText(IDNickname[8]);
					}
					if (Integer.parseInt(IDNickname[1]) > 9) {
						waitingPlayerFrame.lblNickname3.setText(IDNickname[10]);
					}

					if (playersReady[2].equals("v")) {
						waitingPlayerFrame.labelNotReady.setEnabled(false);
						waitingPlayerFrame.labelReady.setEnabled(true);
					}
					if (playersReady[3].equals("v")) {
						waitingPlayerFrame.labelNotReady1.setEnabled(false);
						waitingPlayerFrame.labelReady1.setEnabled(true);
					}
					if (playersReady[4].equals("v")) {
						waitingPlayerFrame.labelNotReady2.setEnabled(false);
						waitingPlayerFrame.labelReady2.setEnabled(true);
					}
					if (playersReady[5].equals("v")) {
						waitingPlayerFrame.labelNotReady3.setEnabled(false);
						waitingPlayerFrame.labelReady3.setEnabled(true);
					}
					waitingPlayerFrame.setPlayers(IDNickname);
					waitingPlayerFrame.setPlayersReady(playersReady);

					waitingPlayerFrame.validate();
					waitingPlayerFrame.repaint();

					break;
				case "Pronto1":
					
					playersReady[3] = "v";
					setChanged();
					notifyObservers(playersReady);
					setChanged();
					notifyObservers("updateRequestPlayers");
					setChanged();
					notifyObservers("updateRequestPlayersReady");
					waitingPlayerFrame.lblNickname.setText(IDNickname[4]);
					if (Integer.parseInt(IDNickname[1]) > 7) {
						waitingPlayerFrame.lblNickname2.setText(IDNickname[8]);
					}
					if (Integer.parseInt(IDNickname[1]) > 9) {
						waitingPlayerFrame.lblNickname3.setText(IDNickname[10]);
					}
					waitingPlayerFrame.setPlayers(IDNickname);
					waitingPlayerFrame.setPlayersReady(playersReady);
					waitingPlayerFrame.repaint();
					setChanged();
					notifyObservers("waiting");
					break;
				case "Pronto2":

					
					playersReady[4] = "v";
					setChanged();
					notifyObservers(playersReady);
					setChanged();
					notifyObservers("updateRequestPlayers");
					setChanged();
					notifyObservers("updateRequestPlayersReady");

					waitingPlayerFrame.lblNickname.setText(IDNickname[4]);
					waitingPlayerFrame.lblNickname1.setText(IDNickname[6]);
					if (Integer.parseInt(IDNickname[1]) > 9) {
						waitingPlayerFrame.lblNickname3.setText(IDNickname[10]);
					}
					waitingPlayerFrame.setPlayers(IDNickname);
					waitingPlayerFrame.setPlayersReady(playersReady);
					waitingPlayerFrame.repaint();
					setChanged();
					notifyObservers("waiting");
					break;
				case "Pronto3":
				

					playersReady[5] = "v";
					setChanged();
					notifyObservers(playersReady);
					setChanged();
					notifyObservers("updateRequestPlayers");
					setChanged();
					notifyObservers("updateRequestPlayersReady");
					waitingPlayerFrame.lblNickname.setText(IDNickname[4]);
					waitingPlayerFrame.lblNickname1.setText(IDNickname[6]);
					waitingPlayerFrame.lblNickname2.setText(IDNickname[8]);
					waitingPlayerFrame.setPlayers(IDNickname);
					waitingPlayerFrame.setPlayersReady(playersReady);
					waitingPlayerFrame.repaint();
					setChanged();
					notifyObservers("waiting");
					break;
				case "Parti":
					
					setChanged();
					notifyObservers("updateRequestPlayers");
					setChanged();
					notifyObservers("updateRequestPlayersReady");
					
					if (prontiPerGiocare()) {
						
						setChanged();
						notifyObservers("broadcastStart");
						// gameBoardFrame = new
						// GameBoardFrame("Sheepland",playersNumber);
						// showJGameWindow(gameBoardFrame);
						// setChanged();
						// notifyObservers(new
						// ViewAction(ViewAction.Command.START));
						// gameBoardFrame
						// .addListenerToJRoads(new SheeplandMouseListener());

					} else {
						JOptionPane.showInternalMessageDialog(
								waitingPlayerFrame,
								"Non ci sono ancora 2 giocatori pronti");

					}
					break;
				default:
					throw new IllegalArgumentException();
				}
			}
			if (e.getSource() instanceof JTextField) {
				JTextField textfield = (JTextField) e.getSource();
				switch (textfield.getName()) {
				case "addplayer":
					addPlayer(textfield);
					break;
				default:
					throw new IllegalArgumentException();
				}
			}

		}

		private void addPlayer(JComponent component) {
			currentFrame.manageButtons(playersNumber);
			String idPlayer = Integer.toString(playersNumber);
			String nickname = ((AddPlayerFrame) SwingUtilities
					.getWindowAncestor(component)).getNickname();
			playersNumber++;
			setChanged();
			notifyObservers(new ViewAction(ViewAction.Command.ADDPLAYER,
					idPlayer, nickname));
		}

		private boolean prontiPerGiocare() {
			int count = 0;
			for (int i = 2; i < 6; i++) {
				if (playersReady[i].equals("v")) {
					count++;
				}
			}
			if (count >= 2) {
				return true;
			} else {
				return false;
			}
		}
	}

	class SheeplandMouseListener implements MouseListener {

		@Override
		public void mouseClicked(MouseEvent e) {
			Object o = e.getSource();
			if (o instanceof JTerrainCard) {
				JTerrainCard jTerrainCard = (JTerrainCard) e.getSource();
				TerrainType cardsType = jTerrainCard.getTerraintype();
				setChanged();
				notifyObservers(new SheeplandAction(
						SheeplandAction.Move.BUYCARD, cardsType.toString()));
			}
			if (o instanceof JRoad) {
				JRoad jRoad = (JRoad) o;
				String jRoadName = jRoad.getName();
				String idRoad = jRoad.getId();
				switch (jRoadName) {
				case "initposition":
					setChanged();
					notifyObservers(new ViewAction(
							ViewAction.Command.SETPOSITION, idRoad));
					break;
				case "jroad":
					setChanged();
					notifyObservers(new SheeplandAction(
							SheeplandAction.Move.MOVESHEPERD, idRoad));

					if (jRoad.hasToken() && jRoad.getToken().isShepherd()) {
						String idPlayer = jRoad.getToken().getIdPlayer();
						String idShepherd = jRoad.getToken().getIdShepherd();
						setChanged();
						notifyObservers(new ViewAction(
								ViewAction.Command.CHOOSESHEP, idPlayer,
								idShepherd));
					}
					break;
				default:
					throw new IllegalArgumentException();
				}
			}
			if (o instanceof JAnimal) {
				JAnimal jAnimal = (JAnimal) o;
				AnimalType animal = jAnimal.getAnimalType();
				JAnimalsPanel jAnimalsPanel = (JAnimalsPanel) jAnimal
						.getParent();
				String idOriginRegion = jAnimalsPanel.getId();
				switch (animal) {
				case SHEEP:
					setChanged();
					notifyObservers(new SheeplandAction(
							SheeplandAction.Move.MOVEANIMAL, animal.toString(),
							idOriginRegion));
					break;
				case BLACKSHEEP:
					setChanged();
					notifyObservers(new SheeplandAction(
							SheeplandAction.Move.MOVEANIMAL, animal.toString(),
							idOriginRegion));
					break;
				default:
					throw new IllegalArgumentException();
				}
			}
		}

		@Override
		public void mouseEntered(MouseEvent e) {
			Object label = e.getSource();
			if (label instanceof JRoad) {
				JRoad jRoad = (JRoad) label;
				jRoad.setCursor(new Cursor(Cursor.HAND_CURSOR));
			}
			if (label instanceof JTerrainCard) {
				JTerrainCard jCard = (JTerrainCard) label;
				jCard.setCursor(new Cursor(Cursor.HAND_CURSOR));
			}
			if (label instanceof JLabel) {
				JLabel jAnimal = (JLabel) label;
				jAnimal.setCursor(new Cursor(Cursor.HAND_CURSOR));
			}
		}

		public void mouseExited(MouseEvent e) {
			Object label = e.getSource();
			if (label instanceof JRoad) {
				JRoad jRoad = (JRoad) label;
				jRoad.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
			}
			if (label instanceof JTerrainCard) {
				JTerrainCard jCard = (JTerrainCard) label;
				jCard.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
			}
			if (label instanceof JAnimal) {
				JAnimal jAnimal = (JAnimal) label;
				jAnimal.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
			}
		}

		@Override
		public void mousePressed(MouseEvent e) {
		}

		@Override
		public void mouseReleased(MouseEvent e) {
		}

	}

}
