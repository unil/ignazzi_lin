package it.polimi.ignazzi_lin.view;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.HeadlessException;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * Schermata iniziale che si presenta all'utente
 * in cui è possibile scegliere se giocare in rete o in locale
 * 
 * @author unil
 *
 */
public class StartingFrame extends JGameFrame {
    
    private JPanel mainPanel;
    
    private JLabel gameLabel;
    
    private ImageIcon gameImage;
    
    private JPanel buttonsPanel;
    
    private JButton multiplayer;
    
    private JButton singleplayer;

    public StartingFrame(String title) throws HeadlessException {
	super(title);
	this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	mainPanel = new JPanel(new BorderLayout());
	buttonsPanel = new JPanel(new GridLayout(0, 1));
	mainPanel.add(buttonsPanel, BorderLayout.PAGE_END);
	gameLabel = new JLabel();
	gameImage = new ImageIcon(getClass().getResource("/sheepland.jpg"));
	gameLabel.setIcon(gameImage);
	mainPanel.add(gameLabel, BorderLayout.PAGE_START);
	
	multiplayer = new JButton("Gioco online");
	singleplayer = new JButton("Gioco in locale");
	multiplayer.setName("online");
	singleplayer.setName("local");
	
	buttonsPanel.add(multiplayer);
	buttonsPanel.add(singleplayer);
	this.add(mainPanel);
	this.pack();
	this.setLocationRelativeTo(null);
    }
    
    public void addListener(ActionListener l) {
	this.singleplayer.addActionListener(l);
	this.multiplayer.addActionListener(l);
    }

}
