package it.polimi.ignazzi_lin.view;

import javax.swing.JLabel;

public class JRoad extends JLabel {
    
    private String id;
    
    private Token token;

    public JRoad(String id) {
	super();
	this.id = id;
	this.token = null;
    }

    public String getId() {
        return id;
    }

    public Token getToken() {
        return token;
    }

    public void setToken(Token token) {
        this.token = token;
    }

    public boolean hasToken() {
	return (token != null) ? true : false;
    }
    
}
