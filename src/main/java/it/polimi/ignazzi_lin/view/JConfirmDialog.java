package it.polimi.ignazzi_lin.view;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * Classe rappresentante una finestra di dialogo che domanda la conferma di un'azione.
 * Ci sono due bottoni: "conferma" e "annulla".
 * 
 * @author unil
 *
 */
public class JConfirmDialog extends JDialog implements ActionListener {
    
    private JButton confirm;
    
    private JButton cancel;
        
    public JConfirmDialog(JFrame parent, String title, String message) {
	super(parent, title, true);
	if (parent != null) {
	    Dimension parentSize = parent.getSize(); 
	    Point p = parent.getLocation(); 
	    setLocation(p.x + parentSize.width / 4, p.y + parentSize.height / 4);
	}
	JPanel messagePane = new JPanel();
	messagePane.add(new JLabel(message));
	getContentPane().add(messagePane);
	JPanel buttonPane = new JPanel();
	this.confirm = new JButton("Conferma"); 
	this.cancel = new JButton("Annulla"); 
	buttonPane.add(confirm);
	buttonPane.add(cancel);
        this.getContentPane().add(buttonPane, BorderLayout.SOUTH);
        this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        this.pack(); 
        this.setVisible(true);
    }
    
    public void addListener(ActionListener act) {
	this.confirm.addActionListener(act);
	this.cancel.addActionListener(act);
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
	String buttonName = e.getActionCommand();
	switch(buttonName) {
	case "Confirm":
	    System.out.println("OK!!");
	    break;
	case "Cancel":
	    System.out.println("Annullato");
	    break;
	default:
	    //TODO
	}
	this.setVisible(false); 
	this.dispose(); 
    }

}
