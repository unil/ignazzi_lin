package it.polimi.ignazzi_lin.view;

import java.awt.FlowLayout;
import java.awt.HeadlessException;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 * Finestra usata dall'utente per scegliere di avviare il gioco in modalità server o client;
 * viene visualizzata dopo aver scelto "Multiplayer" nella schermata iniziale.
 * 
 * @author unil
 *
 */
public class MultiplayerFrame extends JGameFrame {

    private JPanel mainPanel;
    
    private JButton server;
    
    private JButton client;

    public MultiplayerFrame(String title) throws HeadlessException {
	super(title);
	this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	mainPanel = new JPanel(new FlowLayout());
	server = new JButton("Avvia server");
	client = new JButton("Avvia client");
	server.setName("server");
	client.setName("client");
	mainPanel.add(server);
	mainPanel.add(client);
	this.add(mainPanel);
	this.pack();
	this.setLocationRelativeTo(null);
    }
    
    @Override
    public void addListener(ActionListener l) {
	this.server.addActionListener(l);
	this.client.addActionListener(l);
    }
    
}
