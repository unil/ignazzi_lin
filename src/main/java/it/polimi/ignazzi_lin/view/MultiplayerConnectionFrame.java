package it.polimi.ignazzi_lin.view;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.HeadlessException;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

/**
 * Finestra usata dall'utente per scegliere di avviare il gioco in modalità
 * server o client; viene visualizzata dopo aver scelto "Multiplayer" nella
 * schermata iniziale.
 * 
 * @author Ignazzi Marco / unil
 * 
 */
public class MultiplayerConnectionFrame extends JGameFrame {

	private JTextField txtPort;
	private JTextField txtIp;
	private String portNumber = "nessuna porta è stata scritta";
	private String ip = "nessun ip è stato scritto";
	private JButton btnInvia;
	private JPanel iPPanel;
	private JPanel portPanel;
	private JPanel bottonPanel;
	private JPanel mainPanel;
	private JPanel nicknamePanel;
	private JLabel lblInserisciNickname;
	private JTextField txtNickname;
	private int idPlayer;
	private String[] players;

	public MultiplayerConnectionFrame(String title,int idPlayer) throws HeadlessException {
		super(title);
		setResizable(false);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.idPlayer=idPlayer;
		players= new String[3];
		getContentPane().setLayout(new BoxLayout(getContentPane(), BoxLayout.X_AXIS));

		mainPanel = new JPanel();
		getContentPane().add(mainPanel);
		mainPanel.setLayout(new GridLayout(0, 1, 0, 1));
		
		nicknamePanel = new JPanel();
		nicknamePanel.setLayout(null);
		nicknamePanel.setSize(new Dimension(100, 50));
		nicknamePanel.setPreferredSize(new Dimension(350, 25));
		mainPanel.add(nicknamePanel);
		
		lblInserisciNickname = new JLabel("Inserisci il tuo Nickname:");
		lblInserisciNickname.setPreferredSize(new Dimension(164, 24));
		lblInserisciNickname.setHorizontalAlignment(SwingConstants.CENTER);
		lblInserisciNickname.setBounds(0, 11, 164, 24);
		nicknamePanel.add(lblInserisciNickname);
		
		txtNickname = new JTextField();
		txtNickname.setPreferredSize(new Dimension(156, 25));
		txtNickname.setText("");
		txtNickname.setColumns(20);
		txtNickname.setBounds(179, 13, 156, 20);
		nicknamePanel.add(txtNickname);

		iPPanel = new JPanel();
		iPPanel.setPreferredSize(new Dimension(350, 20));
		iPPanel.setSize(new Dimension(100, 50));
		mainPanel.add(iPPanel);
		iPPanel.setLayout(null);

		JLabel lblInserisciIp = new JLabel("Inserisci IP del server:");
		lblInserisciIp.setPreferredSize(new Dimension(164, 24));
		lblInserisciIp.setBounds(0, 11, 164, 24);
		iPPanel.add(lblInserisciIp);
		lblInserisciIp.setHorizontalAlignment(SwingConstants.CENTER);

		txtIp = new JTextField();
		txtIp.setPreferredSize(new Dimension(156, 25));
		txtIp.setBounds(179, 13, 156, 20);
		iPPanel.add(txtIp);
		txtIp.setText("127.0.0.1");
		txtIp.setColumns(20);

		portPanel = new JPanel();
		portPanel.setPreferredSize(new Dimension(350, 25));
		portPanel.setSize(new Dimension(100, 50));
		mainPanel.add(portPanel);
		portPanel.setLayout(null);

		JLabel lblInserisciIlNumero = new JLabel("Inserisci il numero della porta:");
		lblInserisciIlNumero.setPreferredSize(new Dimension(164, 24));
		lblInserisciIlNumero.setBounds(5, 11, 164, 24);
		portPanel.add(lblInserisciIlNumero);
		lblInserisciIlNumero.setHorizontalAlignment(SwingConstants.CENTER);

		txtPort = new JTextField();
		txtPort.setPreferredSize(new Dimension(156, 25));
		txtPort.setBounds(179, 13, 156, 20);
		txtPort.setHorizontalAlignment(SwingConstants.LEFT);
		portPanel.add(txtPort);
		txtPort.setText("19999");
		txtPort.setColumns(6);

		bottonPanel = new JPanel();
		FlowLayout flowLayout = (FlowLayout) bottonPanel.getLayout();
		flowLayout.setVgap(7);
		bottonPanel.setSize(new Dimension(350, 25));
		mainPanel.add(bottonPanel);

		btnInvia = new JButton("Invia");
		btnInvia.setPreferredSize(new Dimension(57, 24));
		bottonPanel.add(btnInvia);
		btnInvia.setName("send");

		this.pack();
		this.setLocationRelativeTo(null);
	}// chiude il costruttore.

	@Override
	public void addListener(ActionListener l) {
		btnInvia.addActionListener(l);
	}

	public String getIP() {
		ip = txtIp.getText();
		portNumber = txtPort.getText();
		String ipAndPort = ip + ":" + portNumber;
		return ipAndPort;
	}

	public String[] getPlayersOnline() {
		String stringIdPlayer = Integer.toString(idPlayer);
		players[0]= "addplayer";
		players[1]= stringIdPlayer;
		players[2]= txtNickname.getText();
		return players;
	}

}
