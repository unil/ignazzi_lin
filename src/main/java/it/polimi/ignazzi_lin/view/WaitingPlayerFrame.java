package it.polimi.ignazzi_lin.view;


import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;


/**
 * classe che rappresenta la Finestra di attesa dei giocatori, nel gioco online. (simile ad una LOBBY)
 * 
 * @author Ignazzi Marco
 */

public class WaitingPlayerFrame extends JGameFrame{
		
		private JPanel mainPanel;
		private JPanel TextPanel;
		
		private JPanel PlayerPanel;
		protected JLabel lblNickname;
		protected JButton btnPronto;
		
		private JPanel PlayerPanel1;
		protected JLabel lblNickname1;
		protected JButton btnPronto1;
		
		private JPanel PlayerPanel2;
		protected JLabel lblNickname2;
		protected JButton btnPronto2;
		
		private JPanel PlayerPanel3;
		protected JLabel lblNickname3;
		protected JButton btnPronto3;
		
		protected JLabel labelNotReady;
		protected JLabel labelReady;
		protected JLabel labelNotReady1;
		protected JLabel labelReady1;
		protected JLabel labelNotReady2;
		protected JLabel labelReady2;
		protected JLabel labelNotReady3;
		protected JLabel labelReady3;

		private JPanel CountdownPanel;
		private JLabel lblinfoToClick;
		private JButton btnParti;
		
		private int idCorrente;
		private String myNickname;
		private String[] playersReady = new String[6];
		private String[] IDNicknameString;
		private String[] playerOnline= new String[3];
		
		public WaitingPlayerFrame(String title , String[] playerOnline,String[] IDNickname,String[] playersReady) throws HeadlessException {
			super(title);
			int size = Integer.parseInt(IDNickname[1]);
			idCorrente=Integer.parseInt(IDNickname[2]); // non dovrebbe piu essere buggato.
			System.out.println("????????????----IL MIO ID:"+idCorrente);
			IDNicknameString = new String[size];
			this.IDNicknameString=IDNickname;
			this.playersReady=playersReady;
			this.playerOnline=playerOnline;
			setResizable(false);
			myNickname=playerOnline[2];

			this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			getContentPane().setLayout(new BoxLayout(getContentPane(), BoxLayout.X_AXIS));
		
			
			
			mainPanel = new JPanel();
			mainPanel.setName("GrigliaPrincipale\r\n");
			getContentPane().add(mainPanel);
			mainPanel.setLayout(new GridLayout(7, 1, 0, 1));
			
			TextPanel = new JPanel();
			TextPanel.setPreferredSize(new Dimension(400, 10));
			TextPanel.setOpaque(false);
			mainPanel.add(TextPanel);
			TextPanel.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
			
			JLabel lblInAttesaDi = new JLabel("IN ATTESA DI ALMENO 2 GIOCATORI...");
			lblInAttesaDi.setFont(new Font("Arial Black", Font.PLAIN, 12));
			TextPanel.add(lblInAttesaDi);
			
			PlayerPanel = new JPanel();
			PlayerPanel.setOpaque(false);
			PlayerPanel.setPreferredSize(new Dimension(400, 10));
			PlayerPanel.setSize(new Dimension(100, 50));
			mainPanel.add(PlayerPanel);
			PlayerPanel.setLayout(null);
			
			lblNickname = new JLabel("Nickname Player0:");
			lblNickname.setBounds(24, 4, 164, 24);
			btnPronto = new JButton("Pronto");
			btnPronto.setName("Pronto");
			btnPronto.setEnabled(false);
			btnPronto.setBounds(197, 4, 89, 23);
			
			if(idCorrente==0){
				lblNickname.setText(myNickname);
				btnPronto.setEnabled(true);
			}

			for(int i=3;i<size;i++){
				if(IDNicknameString[i]=="1"){
					lblNickname.setText(IDNicknameString[i+1]);
				}
			}
			
			
			PlayerPanel.add(lblNickname);
//			}
			btnPronto.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) { 
					labelNotReady.setEnabled(false);
					labelReady.setEnabled(true);
				}
			});
			
			PlayerPanel.add(btnPronto);
			
			labelNotReady = new JLabel("");
			labelNotReady.setIcon(new ImageIcon(WaitingPlayerFrame.class.getResource("/xRossa.png")));
			labelNotReady.setBounds(300, 6, 20, 22);
			PlayerPanel.add(labelNotReady);
			
			labelReady = new JLabel("");
			labelReady.setEnabled(false);
			labelReady.setIcon(new ImageIcon(WaitingPlayerFrame.class.getResource("/spuntaVerde.png")));
			labelReady.setBounds(335, 4, 20, 24);
			PlayerPanel.add(labelReady);
			
			PlayerPanel1 = new JPanel();
			PlayerPanel1.setOpaque(false);
			mainPanel.add(PlayerPanel1);
			PlayerPanel1.setLayout(null);
			PlayerPanel1.setSize(new Dimension(350, 50));
			PlayerPanel1.setPreferredSize(new Dimension(350, 10));
			
			lblNickname1 = new JLabel("Nickname Player1:");
			lblNickname1.setBounds(24, 4, 164, 24);
			btnPronto1 = new JButton("Pronto");
			btnPronto1.setName("Pronto1");
			btnPronto1.setEnabled(false);
			btnPronto1.setBounds(197, 4, 89, 23);
			
			if(idCorrente==1){
				lblNickname1.setText(myNickname);
				btnPronto1.setEnabled(true);
			}
			
			for(int i=3;i<size;i++){
				if(IDNicknameString[i]=="1"){
					lblNickname1.setText(IDNicknameString[i+1]);
				}
			}
			
			PlayerPanel1.add(lblNickname1);
			btnPronto1.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) { 
					labelNotReady1.setEnabled(false);
					labelReady1.setEnabled(true);
				}
			});
			
			PlayerPanel1.add(btnPronto1);
			
			labelNotReady1 = new JLabel("");
			labelNotReady1.setIcon(new ImageIcon(WaitingPlayerFrame.class.getResource("/xRossa.png")));
			labelNotReady1.setBounds(300, 6, 20, 22);
			PlayerPanel1.add(labelNotReady1);
			
			labelReady1 = new JLabel("");
			labelReady1.setEnabled(false);
			labelReady1.setIcon(new ImageIcon(WaitingPlayerFrame.class.getResource("/spuntaVerde.png")));
			labelReady1.setBounds(335, 4, 20, 24);
			PlayerPanel1.add(labelReady1);
			
			PlayerPanel2 = new JPanel();
			PlayerPanel2.setOpaque(false);
			PlayerPanel2.setLayout(null);
			PlayerPanel2.setSize(new Dimension(100, 50));
			PlayerPanel2.setPreferredSize(new Dimension(400, 10));
			mainPanel.add(PlayerPanel2);
			
			lblNickname2 = new JLabel("Nickname Player2:");
			lblNickname2.setBounds(24, 4, 164, 24);
			btnPronto2 = new JButton("Pronto");
			btnPronto2.setName("Pronto2");
			btnPronto2.setEnabled(false);
			btnPronto2.setBounds(197, 4, 89, 23);
			
			if(idCorrente==2){
				lblNickname2.setText(myNickname);
				btnPronto2.setEnabled(true);
			}

			for(int i=3;i<size;i++){
				if(IDNicknameString[i]=="2"){
					lblNickname2.setText(IDNicknameString[i+1]);
				}
			}
			
			
			PlayerPanel2.add(lblNickname2);
			btnPronto2.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) { 
					labelNotReady2.setEnabled(false);
					labelReady2.setEnabled(true);
				}
			});
			PlayerPanel2.add(btnPronto2);
			
			labelNotReady2 = new JLabel("");
			labelNotReady2.setIcon(new ImageIcon(WaitingPlayerFrame.class.getResource("/xRossa.png")));
			labelNotReady2.setBounds(300, 6, 20, 22);
			PlayerPanel2.add(labelNotReady2);
			
			labelReady2 = new JLabel("");
			labelReady2.setEnabled(false);
			labelReady2.setIcon(new ImageIcon(WaitingPlayerFrame.class.getResource("/spuntaVerde.png")));
			labelReady2.setBounds(335, 4, 20, 24);
			PlayerPanel2.add(labelReady2);
			
			PlayerPanel3 = new JPanel();
			PlayerPanel3.setOpaque(false);
			PlayerPanel3.setLayout(null);
			PlayerPanel3.setSize(new Dimension(100, 50));
			PlayerPanel3.setPreferredSize(new Dimension(400, 10));
			mainPanel.add(PlayerPanel3);
			
			lblNickname3 = new JLabel("Nickname Player3:");
			lblNickname3.setBounds(24, 4, 164, 24);
			btnPronto3 = new JButton("Pronto");
			btnPronto3.setName("Pronto3");
			btnPronto3.setEnabled(false);
			btnPronto3.setBounds(197, 4, 89, 23);
			
			if(idCorrente==3){
				lblNickname3.setText(myNickname);
				btnPronto3.setEnabled(true);
			}

			for(int i=3;i<size;i++){
				if(IDNicknameString[i]=="3"){
					lblNickname3.setText(IDNicknameString[i+1]);
				}
			}
			
			PlayerPanel3.add(lblNickname3);
			btnPronto3.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) { 
					labelNotReady3.setEnabled(false);
					labelReady3.setEnabled(true);
				}
			});
			PlayerPanel3.add(btnPronto3);
			
			labelNotReady3 = new JLabel("");
			labelNotReady3.setIcon(new ImageIcon(WaitingPlayerFrame.class.getResource("/xRossa.png")));
			labelNotReady3.setBounds(300, 6, 20, 22);
			PlayerPanel3.add(labelNotReady3);
			
			labelReady3 = new JLabel("");
			labelReady3.setEnabled(false);
			labelReady3.setIcon(new ImageIcon(WaitingPlayerFrame.class.getResource("/spuntaVerde.png")));
			labelReady3.setBounds(335, 4, 20, 24);
			PlayerPanel3.add(labelReady3);

			CountdownPanel = new JPanel();
			CountdownPanel.setPreferredSize(new Dimension(400, 32));
			CountdownPanel.setOpaque(false);
			CountdownPanel.setSize(new Dimension(100, 50));
			mainPanel.add(CountdownPanel);
			CountdownPanel.setLayout(new FlowLayout(FlowLayout.CENTER, 24, 10));
			
			lblinfoToClick = new JLabel("Quando 2 giocatori risultano pronti,si può partire!");
			lblinfoToClick.setFont(new Font("Tahoma", Font.BOLD, 10));
			CountdownPanel.add(lblinfoToClick);
			
			btnParti = new JButton("Parti!");
			btnParti.setPreferredSize(new Dimension(400, 23));
			btnParti.setEnabled(true);
			btnParti.setOpaque(false);
			mainPanel.add(btnParti);
			btnParti.setName("Parti");

			this.pack();
			this.setLocationRelativeTo(null);
		}// chiude il costruttore.

		@Override
		public void addListener(ActionListener l) {
			btnPronto.addActionListener(l);
			btnPronto1.addActionListener(l);
			btnPronto2.addActionListener(l);
			btnPronto3.addActionListener(l);
			btnParti.addActionListener(l);
		}

		public void setPlayers(String[] IDNicknameUpdated) {
			this.IDNicknameString=IDNicknameUpdated;
			
		}

		public void setPlayersReady(String[] playersReady) {
			this.playersReady=playersReady;
		}
		
		public void updateFrame(String[] playersReady , String[] IDNicknameUpdated) {
			this.playersReady=playersReady;
			this.IDNicknameString=IDNicknameUpdated;
			
		}

}
