package it.polimi.ignazzi_lin.view;

import it.polimi.ignazzi_lin.model.AnimalType;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class JAnimalsPanel extends JPanel {

    private String idRegion;

    private HashMap<AnimalType, Integer> animals = new HashMap<AnimalType, Integer>();

    private HashMap<AnimalType, JAnimal> jAnimals = new HashMap<AnimalType, JAnimal>();

    public JAnimalsPanel(String id) {
	super();

	this.idRegion = id;

	this.setLayout(new GridLayout(2, 1));
	try {
	    for (AnimalType type : AnimalType.values()) {
		animals.put(type, 0);
	    }

	    BufferedImage sheepImage = null;
	    sheepImage = ImageIO.read(getClass().getResource(
		    "/sheep.png"));
	    sheepImage = GUI.createResizedCopy(sheepImage, 22, 22, true);
	    JAnimal sheepLabel = new JAnimal(new ImageIcon(sheepImage), AnimalType.SHEEP);
	    jAnimals.put(AnimalType.SHEEP, sheepLabel);
	    sheepLabel.setText(Integer.toString(animals.get(AnimalType.SHEEP)));
	    sheepLabel.setFont(new Font("Serif", Font.BOLD, 14));
	    sheepLabel.setForeground(Color.BLACK);

	    BufferedImage blacksheepImage = null;
	    blacksheepImage = ImageIO.read(getClass().getResource(
		    "/blacksheep.png"));
	    blacksheepImage = GUI.createResizedCopy(blacksheepImage, 22, 22,
		    true);
	    JAnimal blacksheepLabel = new JAnimal(
		    new ImageIcon(blacksheepImage), AnimalType.BLACKSHEEP);
	    jAnimals.put(AnimalType.BLACKSHEEP, blacksheepLabel);
	    blacksheepLabel.setText(Integer.toString(animals
		    .get(AnimalType.BLACKSHEEP)));
	    blacksheepLabel.setFont(new Font("Serif", Font.BOLD, 14));
	    blacksheepLabel.setForeground(Color.BLACK);
	    sheepLabel.setName(AnimalType.BLACKSHEEP.toString());

	    jAnimals.get(AnimalType.SHEEP).setVisible(false);
	    jAnimals.get(AnimalType.BLACKSHEEP).setVisible(false);
	    this.setOpaque(false);
	    this.add(sheepLabel);
	    this.add(blacksheepLabel);
	} catch (IOException e) {
	    JOptionPane.showMessageDialog(this, "Errore di lettura dati!");
	}
    }

    public void addAnimal(AnimalType type) {
	animals.put(type, animals.get(type) + 1);
	jAnimals.get(type).setText(Integer.toString(animals.get(type)));
	if (animals.get(type) <= 0) {
	    jAnimals.get(type).setVisible(false);
	    jAnimals.get(type).setEnabled(false);
	} else {
	    jAnimals.get(type).setVisible(true);
	    jAnimals.get(type).setEnabled(true);
	}
    }

    public void removeAnimal(AnimalType type) {
	animals.put(type, animals.get(type) - 1);
	jAnimals.get(type).setText(Integer.toString(animals.get(type)));
	if (animals.get(type) <= 0) {
	    jAnimals.get(type).setVisible(false);
	    jAnimals.get(type).setEnabled(false);
	} else {
	    jAnimals.get(type).setVisible(true);
	    jAnimals.get(type).setEnabled(true);
	}
    }

    public String getId() {
	return idRegion;
    }

    public HashMap<AnimalType, Integer> getAnimals() {
	return animals;
    }

    public void addSheeplandListener(MouseListener l) {
	for (Map.Entry<AnimalType, JAnimal> jAnimalEntry : jAnimals.entrySet()) {
	    jAnimalEntry.getValue().addMouseListener(l);
	}

    }

}
