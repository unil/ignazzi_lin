package it.polimi.ignazzi_lin.view;

import it.polimi.ignazzi_lin.model.AnimalType;

import javax.swing.Icon;
import javax.swing.JLabel;

public class JAnimal extends JLabel {
    
    private AnimalType animalType;

    public JAnimal(AnimalType animalType) {
	super();
	this.animalType = animalType;
    }

    public JAnimal(Icon image, AnimalType animalType) {
	super(image);
	this.animalType = animalType;
    }

    public AnimalType getAnimalType() {
	return animalType;
    }

}
