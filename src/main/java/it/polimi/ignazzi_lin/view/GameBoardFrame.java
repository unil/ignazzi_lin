package it.polimi.ignazzi_lin.view;

import it.polimi.ignazzi_lin.model.AnimalType;
import it.polimi.ignazzi_lin.model.Constants;
import it.polimi.ignazzi_lin.model.TerrainType;
import it.polimi.ignazzi_lin.model.XmlParser;
import it.polimi.ignazzi_lin.view.GUI.SheeplandMouseListener;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.HeadlessException;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingWorker;
import javax.swing.border.EtchedBorder;

/**
 * Schermata principale contenente mappa di gioco, pedine, tessere e tutto il
 * necessario per giocare.
 * 
 * @author unil
 * 
 */
public class GameBoardFrame extends JGameFrame {

    private XmlParser XMLParser;

    private ArrayList<TerrainType> terrainTypes = new ArrayList<TerrainType>();

    private JPanel mainPanel;

    private JPanel cardsPanel;

    private JPanel playerInfoPanel;

    private JLabel jMessage;

    private ArrayList<JTerrainCard> jTerrainCards = new ArrayList<JTerrainCard>();

    private ArrayList<JRoad> jRoads = new ArrayList<JRoad>();

    private ArrayList<JLabel> jPlayers = new ArrayList<JLabel>();

    private ArrayList<Token> jShepherds = new ArrayList<Token>();

    private ArrayList<JAnimalsPanel> jAnimalsPanels = new ArrayList<JAnimalsPanel>();

    private ArrayList<JLabel> jGoldCounters = new ArrayList<JLabel>();

    BufferedImage mapImage;

    private float mapWidth;

    private float mapHeight;

    private float newMapHeight;

    private float newMapWidth;

    private JLabel map;

    private JPanel mapPanel;

    private Token fence;

    private Token fenceFinal;

    private JLabel jFence;

    private JLabel animationLabel;

    /**
     * Realizza la schermata di gioco principale con la mappa, le tessere, le
     * info giocatore etc.
     * 
     * @param title
     *            Titolo del frame.
     */
    public GameBoardFrame(String title, int playersNumber)
	    throws HeadlessException {
	super(title);
	this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

	Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
	XMLParser = new XmlParser();
	terrainTypes = XMLParser.getTerrainTypes();

	mainPanel = new JPanel(new BorderLayout());

	try {
	    mapImage = ImageIO.read(getClass().getResource(
		    "/" + XMLParser.getMapImageName()));
	} catch (Exception e) {
	    JOptionPane.showMessageDialog(this, "Errore di lettura dati!");
	}
	mapHeight = mapImage.getHeight();
	mapWidth = mapImage.getWidth();
	newMapHeight = screenSize.height * 0.8f;
	newMapWidth = mapImage.getWidth() * newMapHeight / mapImage.getHeight();
	BufferedImage mapImageScaled = GUI.createResizedCopy(mapImage,
		(int) newMapWidth, (int) newMapHeight, true);
	map = new JLabel(new ImageIcon(mapImageScaled));
	map.setLayout(null);

	mapPanel = new JPanel();
	mapPanel.add(map);

	animationLabel = new JLabel();
	map.add(animationLabel);

	jMessage = new JLabel();
	jMessage.setFont(new Font("Serif", Font.BOLD, 14));
	jMessage.setForeground(Color.ORANGE);
	jMessage.setBackground(Color.DARK_GRAY);
	jMessage.setOpaque(true);
	jMessage.setBorder(BorderFactory.createLineBorder(Color.GREEN));
	jMessage.setVisible(false);
	map.add(jMessage);

	int cardsNumber = terrainTypes.size();
	cardsPanel = new JPanel(new GridLayout(cardsNumber, 1, 4, 0));
	float cardHeight = (newMapHeight / cardsNumber) - 4;
	for (int i = 0; i < cardsNumber; i++) {
	    try {
		BufferedImage cardImage = ImageIO.read(getClass().getResource(
			"/type" + terrainTypes.get(i).toString().toLowerCase()
				+ ".png"));
		float cardWidth = cardImage.getWidth() * cardHeight
			/ cardImage.getHeight();
		BufferedImage cardImageScaled = GUI.createResizedCopy(
			cardImage, (int) cardWidth, (int) cardHeight, true);
		JTerrainCard jTerrainCard = new JTerrainCard(new ImageIcon(
			cardImageScaled));
		jTerrainCards.add(jTerrainCard);
		jTerrainCard.setTerraintype(terrainTypes.get(i));

		jTerrainCard.setLayout(new BoxLayout(jTerrainCard,
			BoxLayout.X_AXIS));
		JPanel pricePanel = new JPanel();
		pricePanel
			.setLayout(new BoxLayout(pricePanel, BoxLayout.Y_AXIS));
		JPanel counterPanel = new JPanel();
		counterPanel.setLayout(new BoxLayout(counterPanel,
			BoxLayout.Y_AXIS));
		BufferedImage jPriceLabelImage = ImageIO.read(getClass()
			.getResource("/priceLabel.png"));
		JLabel jPriceLabel = new JLabel(new ImageIcon(jPriceLabelImage));
		jPriceLabel.setLayout(new BoxLayout(jPriceLabel,
			BoxLayout.X_AXIS));
		jPriceLabel.setFont(new Font("Serif", Font.BOLD, 16));
		jPriceLabel.setForeground(Color.BLACK);
		jPriceLabel.setHorizontalTextPosition(JLabel.CENTER);
		BufferedImage jCardsCounterLabelImage = ImageIO.read(getClass()
			.getResource("/counterLabel.png"));
		JLabel jCardsCounterLabel = new JLabel(new ImageIcon(
			jCardsCounterLabelImage));
		jCardsCounterLabel.setLayout(new BoxLayout(jCardsCounterLabel,
			BoxLayout.X_AXIS));
		jCardsCounterLabel.setFont(new Font("Serif", Font.BOLD, 16));
		jCardsCounterLabel.setForeground(Color.WHITE);
		jCardsCounterLabel.setHorizontalTextPosition(JLabel.CENTER);
		pricePanel.setOpaque(false);
		counterPanel.setOpaque(false);
		jPriceLabel.setAlignmentX(1.0f);
		jCardsCounterLabel.setAlignmentX(0.0f);
		jCardsCounterLabel.setAlignmentY(1.0f);
		pricePanel.add(jPriceLabel);
		pricePanel.add(Box.createVerticalGlue());
		counterPanel.add(Box.createVerticalGlue());
		counterPanel.add(jCardsCounterLabel);

		jTerrainCard.setjPriceLabel(jPriceLabel);
		jTerrainCard.setjCardsCounterLabel(jCardsCounterLabel);
		jTerrainCard.setTextPrice(Integer
			.toString(Constants.CARD_PRICE));
		jTerrainCard.setTextCount("0");
		jTerrainCard.add(pricePanel);
		jTerrainCard.add(Box.createHorizontalGlue());
		jTerrainCard.add(counterPanel);

		cardsPanel.add(jTerrainCard);
	    } catch (IOException e) {
		JOptionPane.showMessageDialog(this, "Errore di lettura dati!");
	    }
	}

	playerInfoPanel = new JPanel(new GridLayout(playersNumber, 1, 20, 0));
	float pWidth = newMapWidth / 2;
	for (int i = 0; i < playersNumber; i++) {
	    try {
		BufferedImage playerImage = ImageIO.read(getClass()
			.getResource(
				"/player" + Integer.toString(i + 1) + ".png"));
		float pHeight = playerImage.getHeight() * pWidth
			/ playerImage.getWidth();
		BufferedImage playerImageScaled = GUI.createResizedCopy(
			playerImage, (int) pWidth, (int) pHeight, true);
		JLabel jPlayer = new JLabel(new ImageIcon(playerImageScaled));
		jPlayer.setLayout(new BoxLayout(jPlayer, BoxLayout.Y_AXIS));
		JLabel jGold = new JLabel();
		jGold.setAlignmentX(0.0f);
		jGold.setFont(new Font("Serif", Font.BOLD, 14));
		jGold.setForeground(Color.YELLOW);
		jGoldCounters.add(jGold);
		jPlayer.add(jGold);
		jPlayer.add(Box.createVerticalGlue());

		jPlayers.add(jPlayer);
		playerInfoPanel.add(jPlayer);
	    } catch (IOException e) {
		JOptionPane.showMessageDialog(this, "Errore di lettura dati!");
	    }
	}
	BufferedImage fenceImage = null;
	BufferedImage fenceFinalImage = null;
	try {
	    fenceImage = ImageIO.read(getClass().getResource("/fence.png"));
	    jFence = new JLabel(new ImageIcon(fenceImage));
	    fenceImage = GUI.createResizedCopy(fenceImage, 25, 25, true);
	    fenceFinalImage = ImageIO.read(getClass().getResource(
		    "/final_fence.png"));
	    fenceFinalImage = GUI.createResizedCopy(fenceFinalImage, 25, 25,
		    true);

	    fence = new Token(fenceImage);
	    fenceFinal = new Token(fenceFinalImage);

	    jFence.setHorizontalTextPosition(JLabel.CENTER);
	    jFence.setVerticalTextPosition(JLabel.BOTTOM);
	    jFence.setFont(new Font("Serif", Font.ITALIC, 14));
	    jFence.setForeground(Color.BLACK);
	    jFence.setText(Integer.toString(Constants.FENCE_NUMBER));
	    jFence.setBounds(15, 8, jFence.getPreferredSize().width,
		    jFence.getPreferredSize().height);
	    map.add(jFence);

	} catch (IOException e) {
	    JOptionPane.showMessageDialog(this, "Errore di lettura dati!");
	}

	cardsPanel.setBackground(new Color(35, 159, 247));
	mapPanel.setBackground(new Color(35, 159, 247));
	playerInfoPanel.setBackground(new Color(35, 159, 247));
	playerInfoPanel.setVisible(true);

	mainPanel.add(playerInfoPanel, BorderLayout.EAST);
	mainPanel.add(cardsPanel, BorderLayout.WEST);
	mainPanel.add(mapPanel, BorderLayout.CENTER);

	this.add(mainPanel);
	this.pack();

	this.setLocationRelativeTo(null);
    }

    public void addListenerToJRoads(MouseListener l) {
	if (l instanceof SheeplandMouseListener) {
	    for (JRoad jRoad : jRoads) {
		jRoad.addMouseListener(l);
	    }
	}
    }

    public void addListenerToJCards(MouseListener l) {
	if (l instanceof SheeplandMouseListener) {
	    for (JTerrainCard jTerrainCard : jTerrainCards) {
		jTerrainCard.addMouseListener(l);
	    }
	}
    }

    public void addListenerToJAnimals(MouseListener l) {
	if (l instanceof SheeplandMouseListener) {
	    for (JAnimalsPanel jAnimalsPanel : jAnimalsPanels) {
		jAnimalsPanel.addSheeplandListener(l);
	    }
	}
    }

    /**
     * Aggiunge alla mappa tutte le JRoad, rappresentano le strade cliccabili.
     * 
     * @param allX
     *            ArrayList di coordinate X.
     * @param allY
     *            ArrayList di coordinate Y.
     * @param allIDs
     *            ArrayList di id Road.
     */
    public void addJRoadsToMap(ArrayList<Integer> allX,
	    ArrayList<Integer> allY, ArrayList<String> allIDs) {
	for (int i = 0; i < allIDs.size(); i++) {
	    JRoad jRoad = new JRoad(allIDs.get(i));
	    float x = allX.get(i) * newMapWidth / mapWidth;
	    float y = allY.get(i) * newMapHeight / mapHeight;
	    jRoad.setBounds((int) x, (int) y, 25, 25);
	    jRoads.add(jRoad);
	    jRoad.setName("initposition");
	    map.add(jRoad);
	}
    }

    public void setJRoadsName(String name) {
	for (JRoad jRoad : jRoads) {
	    jRoad.setName("jroad");
	}
    }

    /**
     * Aggiunge le immagini degli animali alla mappa.
     * 
     * @param allX
     *            ArrayList di coordinate X.
     * @param allY
     *            ArrayList di coordinate Y.
     * @param allIDs
     *            ArrayList di id Road.
     */
    public void addJAnimals(ArrayList<Integer> allX, ArrayList<Integer> allY,
	    ArrayList<String> allIDs) {
	for (int i = 0; i < allIDs.size(); i++) {
	    JAnimalsPanel jAnimal = new JAnimalsPanel(allIDs.get(i));
	    float x = allX.get(i) * newMapWidth / mapWidth;
	    float y = allY.get(i) * newMapHeight / mapHeight;
	    jAnimal.setBounds((int) x, (int) y,
		    jAnimal.getPreferredSize().width,
		    jAnimal.getPreferredSize().height);
	    if (i == 0) {
		jAnimal.addAnimal(AnimalType.BLACKSHEEP);
	    } else {
		jAnimal.addAnimal(AnimalType.SHEEP);
	    }
	    jAnimalsPanels.add(jAnimal);
	    map.add(jAnimal);
	}
    }

    /**
     * Usata quando bisogna selezionare la posizione iniziale del pastore. La
     * pedina è rappresentata dalla classe Token.
     * 
     * @param idPlayer
     *            id del giocatore che possiede la pedina pastore.
     * @param idSheperd
     *            id del pastore-pedina.
     * @param idRoad
     *            id della strada su cui posizionare il pastore
     * @param color
     *            colore della pedina.
     */
    public void setTokenPosition(String idPlayer, String idSheperd,
	    String idRoad, String color) {
	BufferedImage shepherdImage = null;
	try {
	    shepherdImage = ImageIO.read(getClass().getResource(
		    "/shepherd_" + color + ".png"));
	    shepherdImage = GUI.createResizedCopy(shepherdImage, 25, 25, true);
	} catch (Exception e) {
	    JOptionPane.showMessageDialog(this, "Errore di lettura dati!");
	}

	Token token = new Token(shepherdImage, idPlayer, idSheperd, idRoad);
	this.jShepherds.add(token);
	JRoad jRoad = jRoads.get(Integer.parseInt(idRoad));
	jRoad.setToken(token);
	token.setjRoad(jRoad);
	jRoad.setIcon(token);
    }

    public void showGoldCounters(String initialGold) {
	for (JLabel jGold : jGoldCounters) {
	    jGold.setText(initialGold);
	}
    }

    public void playerSpendGold(String idPlayer, String gold) {
	jGoldCounters.get(Integer.parseInt(idPlayer)).setText(gold);
    }

    public void moveJShepherd(String idPlayer, String idShepherd,
	    String idRoad, int fences) {
	boolean useFinalFences = (fences <= 0) ? true : false;
	jFence.setText(Integer.toString(fences));
	for (Token jShepherd : jShepherds) {
	    if (idPlayer.equals(jShepherd.getIdPlayer())
		    && idShepherd.equals(jShepherd.getIdShepherd())) {
		String originRoad = jShepherd.getPosition();
		JRoad jOriginRoad = jRoads.get(Integer.parseInt(originRoad));
		if (useFinalFences) {
		    jOriginRoad.setIcon(fenceFinal);
		    jOriginRoad.setToken(fenceFinal);
		} else {
		    jOriginRoad.setIcon(fence);
		    jOriginRoad.setToken(fence);
		}
		JRoad jRoadToMove = jRoads.get(Integer.parseInt(idRoad));
		shepherdAnimation(jShepherd, jOriginRoad, jRoadToMove);
		break;
	    }
	}
    }

    public void showBuyCard(TerrainType terrainType, String price, String count) {
	for (JTerrainCard jTerrainCard : jTerrainCards) {
	    if (jTerrainCard.getTerraintype().equals(terrainType)) {
		jTerrainCard.setTextPrice(price);
		jTerrainCard.setTextCount(count);
	    }
	}
    }

    public void moveJAnimal(AnimalType animal, String idOriginRegion,
	    String idDestinationRegion) {
	jAnimalsPanels.get(Integer.parseInt(idOriginRegion)).removeAnimal(
		animal);
	jAnimalsPanels.get(Integer.parseInt(idDestinationRegion)).addAnimal(
		animal);
    }

    public void nextPlayer(String currentPlayerID, String nextPlayerID) {
	JLabel jCurrentPlayer = jPlayers.get(Integer.parseInt(currentPlayerID));
	jCurrentPlayer.setBorder(null);
	jCurrentPlayer.setBackground(null);
	JLabel jNextPlayer = jPlayers.get(Integer.parseInt(nextPlayerID));
	jNextPlayer.setBorder(BorderFactory
		.createEtchedBorder(EtchedBorder.LOWERED));
	jNextPlayer.setBackground(Color.BLUE);
    }

    public void showMessage(String message) {
	jMessage.setText(message);
	int offsetX = (int) (mapWidth - jMessage.getPreferredSize().width) / 2;
	jMessage.setBounds(offsetX, 5, jMessage.getPreferredSize().width,
		jMessage.getPreferredSize().height);
	jMessage.setVisible(true);
    }

    /**
     * Implementa l'animazione della pedina del pastore.
     */
    public void shepherdAnimation(final Token jShepherd, JRoad originJRoad,
	    final JRoad destinationJRoad) {
	final Point start = new Point(originJRoad.getLocation().x,
		originJRoad.getLocation().y);
	final Point destination = new Point(destinationJRoad.getLocation().x,
		destinationJRoad.getLocation().y);
	animationLabel.setIcon(jShepherd);
	animationLabel.setBounds(start.x, start.y,
		animationLabel.getPreferredSize().height,
		animationLabel.getPreferredSize().width);
	animationLabel.setVisible(true);
	BufferedImage imageJShep = null;
	int imageSize = 0;
	imageJShep = (BufferedImage) jShepherd.getImage();
	imageSize = imageJShep.getHeight();
	final BufferedImage imageJShepherd = imageJShep;
	final int size = imageSize;
	SwingWorker<Integer, String> sw = new SwingWorker<Integer, String>() {

	    @Override
	    protected Integer doInBackground() throws Exception {
		setEnabled(false);
		int x = 0;
		int y = 0;
		float rate = 1f;
		for (float i = 0; i < 1; i += 0.01f) {
		    x = start.x
			    + (int) Math.round((destination.x - start.x) * i);
		    y = start.y
			    + (int) Math.round((destination.y - start.y) * i);
		    if (i < 0.5f) {
			animationLabel.setIcon(new ImageIcon(GUI
				.createResizedCopy(imageJShepherd,
					(int) (size * rate),
					(int) (size * rate), true)));
			animationLabel.setBounds(x, y,
				animationLabel.getPreferredSize().width,
				animationLabel.getPreferredSize().height);
			rate += 0.02f;
		    } else if (i == 0.5f) {
			rate = 2f;
		    } else if (i > 0.5f) {
			animationLabel.setIcon(new ImageIcon(GUI
				.createResizedCopy(imageJShepherd,
					(int) (size * rate),
					(int) (size * rate), true)));
			animationLabel.setBounds(x, y,
				animationLabel.getPreferredSize().width,
				animationLabel.getPreferredSize().height);
			rate -= 0.02f;
		    }
		    Thread.sleep(10);
		}
		animationLabel.setVisible(false);
		destinationJRoad.setIcon(jShepherd);
		destinationJRoad.setToken(jShepherd);
		jShepherd.setjRoad(destinationJRoad);
		jShepherd.setPosition(destinationJRoad.getId());
		setEnabled(true);
		return null;
	    }
	};

	sw.execute();
    }

}
