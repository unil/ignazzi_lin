package it.polimi.ignazzi_lin.view;

import it.polimi.ignazzi_lin.model.Model;

import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

public abstract class View extends Observable implements Observer {

    public View() {
    }
    
    protected void showJGameWindow(JGameFrame jGameWindow) {
    }
    
    public void run() {
    }
    
    protected void showModel(Model model, Object arg) {
    }

    public void showMultiplayerFrame() {
    }
    
    public void initializeJRoads(ArrayList<Integer> allX,
	    ArrayList<Integer> allY, ArrayList<String> allIDs) {
    }

    public void initializeJAnimals(ArrayList<Integer> allXregion,
	    ArrayList<Integer> allYregion, ArrayList<String> allIDsregion) {
    }

    public void showMessage(String message) {
    }

    public void setJRoadsName(String name) {
    }

    public void nextPlayer(String currentPlayerID, String nextPlayer) {
    }

    public void addListenerCardsAnimals() {
    }

    public void showGoldCounters(String playerGold) {
    }

    public void JOptionPane(String string) {
    }

}
