package it.polimi.ignazzi_lin.view;

import it.polimi.ignazzi_lin.model.TerrainType;

import javax.swing.Icon;
import javax.swing.JLabel;

public class JTerrainCard extends JLabel {

    private TerrainType terraintype;

    private JLabel jPriceLabel;

    private JLabel jCardsCounterLabel;

    public JTerrainCard(Icon image) {
	super(image);
    }

    public TerrainType getTerraintype() {
	return terraintype;
    }

    public void setTerraintype(TerrainType terraintype) {
	this.terraintype = terraintype;
    }

    public void setTextPrice(String price) {
	jPriceLabel.setText(price);
    }

    public void setTextCount(String count) {
	jCardsCounterLabel.setText(count);
    }

    public JLabel getjPriceLabel() {
	return jPriceLabel;
    }

    public void setjPriceLabel(JLabel jPriceLabel) {
	this.jPriceLabel = jPriceLabel;
    }

    public JLabel getjCardsCounterLabel() {
	return jCardsCounterLabel;
    }

    public void setjCardsCounterLabel(JLabel jCardsCounterLabel) {
	this.jCardsCounterLabel = jCardsCounterLabel;
    }

}
