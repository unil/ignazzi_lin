package it.polimi.ignazzi_lin.model;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Classe che rappresenta il pastore e il giocatore.
 * 
 * @author unil
 *
 */
public class Shepherd {
    
    private Player playerOwner;

    private String id;
    
    private Road position;
    
    private ArrayList<TerrainCard> cards;
    
    public Shepherd(Player playerOwner, String id, int gold) {
	this.playerOwner = playerOwner;
	this.id = id;
	this.playerOwner.setGold(gold);
	this.cards = playerOwner.getCards();
    }

    /**
     * Muove il pastore dalla posizione ad un'altra strada.
     * 
     *  @param road Strada di destinazione.
     */
    public void moveTo(Road road) {
	if(!(this.position.getNearRoads().contains(road))) {
	    this.payGold(Constants.MOVINGPRICE);
	}
	this.position.putFence();
	this.position = road;
	road.setOccupied();
    }
	
    /**
     * Sottrae al portafoglio il prezzo.
     * 
     * @param price Prezzo da pagare.
     */
    private void payGold(int price) {
	int playersWallet = this.playerOwner.getGold();
	if(price > playersWallet) {
	    throw (new IllegalArgumentException());
	}
	this.playerOwner.setGold(playersWallet - price);
    }
	
    /**
     * Compra una tessera di un determinato tipo.
     * 
     * @param type Tipo della tessera che viene acquistata.
     * @param deckList Hash delle pile di tessere da cui acquistare la tessera.
     */
    public void buyCard(TerrainType type, HashMap<TerrainType, ArrayList<TerrainCard>> deckList) {
	ArrayList<TerrainCard> startDeck = deckList.get(type);
	TerrainCard newCard = startDeck.remove(startDeck.size() - 1);
	this.payGold(newCard.getPrice());
	this.cards.add(newCard);
    }
	
    /**
     * Sposta un animale da una regione all'altra.
     * 
     * @param originRegion Regione dove si trova la pecora da spostare.
     */
    public void moveSheep(AnimalType animal, Region originRegion, Region destinationRegion) {
	originRegion.moveAnimal(animal, destinationRegion);
    }
    
    public Player getPlayerOwner() {
        return playerOwner;
    }
    
    public String getId() {
        return id;
    }
    
    public Road getPosition() {
        return position;
    }

    public void setPosition(Road position) {
        this.position = position;
        position.setOccupied();
    }
    
}
