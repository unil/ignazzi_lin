package it.polimi.ignazzi_lin.model;

/**	
 * Classe che rappresenta le tessere terreno.
 * 
 * @author unil
 *
 */
public class TerrainCard {
    
    TerrainType type;
    
    private int price;
	
    /**
     * @param type Tipo della tessera terreno.
     * @param price Prezzo in oro della tessera.
     */
    public TerrainCard(TerrainType type, int price) {
	super();
	this.type = type;
	this.price = price;
    }

    public TerrainType getType() {
	return type;
    }

    public int getPrice() {
	return price;
    }
    
}
