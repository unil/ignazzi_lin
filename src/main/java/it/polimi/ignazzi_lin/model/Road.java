package it.polimi.ignazzi_lin.model;

import java.util.ArrayList;

public class Road {
    
    private String id;
    
    private int diceNumber;

    private ArrayList<Region> nearRegions;

    private ArrayList<Road> nearRoads;
    
    private boolean occupied;
    
    private boolean fence;
    
    private String coordinates;
    
    public Road(String id, int diceNumber, String coordinates) {
	this.id = id;
	this.diceNumber = diceNumber;
	this.coordinates = coordinates;
	this.occupied = false;
	this.fence = false;
    }

    /**
     * Usato dalla classe Sheperd per mettere un recinto dopo ogni movimento.
     */
    public void putFence() {
	this.setOccupied();
	this.fence = true;
    }
    
    public boolean hasFence() {
	return this.fence;
    }
    
    public boolean isOccupied() {
	return this.occupied;
    }
    
    public void setOccupied() {
	this.occupied = true;
    }
    
    public String getId() {
        return id;
    }
        
    public int getDiceNumber() {
        return diceNumber;
    }

    public ArrayList<Region> getNearRegions() {
        return nearRegions;
    }
    
    public void setNearRegions(ArrayList<Region> nearRegions) {
        this.nearRegions = nearRegions;
    }

    public ArrayList<Road> getNearRoads() {
        return nearRoads;
    }
    
    public void setNearRoads(ArrayList<Road> nearRoads) {
        this.nearRoads = nearRoads;
    }
    
    public int getX() {
	return Integer.parseInt(coordinates.split("x")[0]);
    }
    
    public int getY() {
	return Integer.parseInt(coordinates.split("x")[1]);
    }

}