package it.polimi.ignazzi_lin.model;

import it.polimi.ignazzi_lin.controller.SheeplandAction;
import it.polimi.ignazzi_lin.controller.ViewAction;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Observable;

public class Model extends Observable {

    private ArrayList<TerrainType> terrainTypes;

    private ArrayList<Region> regions;

    private ArrayList<Road> roads;

    private ArrayList<Player> players = new ArrayList<Player>();
    
    private String[] IDNickname;
    
	private String[] broadcastString;

	private String[] playersSize;

    private int fences = Constants.FENCE_NUMBER;

    /**
     * Hash di liste di tessere.
     */
    private HashMap<TerrainType, ArrayList<TerrainCard>> deckList = new HashMap<TerrainType, ArrayList<TerrainCard>>();

    private Dice dice;

    private String[] playersReady;

    private String IDAsker;

    /**
     * @param deckDimension
     *            Estensione delle liste da creare.
     */
    public Model(int deckDimension, int diceFaces, ArrayList<Road> roads,
	    ArrayList<Region> regions, ArrayList<TerrainType> terrainTypes) {
	this.roads = roads;
	this.regions = regions;
	this.terrainTypes = terrainTypes;
	this.createDecks(deckDimension);
	this.dice = new Dice(diceFaces);
	playersReady = new String[6];
	playersReady[0] = "stringReady";
	for (int i = 2; i < 6; i++) {
	    playersReady[i] = "0";
	}
	IDNickname = new String[11];
    }

    /**
     * Aggiunge a deckList un array di tessere per ogni tipo.
     * 
     * @param length
     *            Estensione degli array da creare.
     */
    protected void createDecks(int length) {
	for (TerrainType type : TerrainType.values()) {
	    if (type != TerrainType.SHEEPSBURG) {
		this.deckList.put(type, createOneTypeDeck(type, length));
	    }
	}
    }

    /**
     * Crea un mazzo di tessere di un certo tipo con prezzo decrescente.
     * 
     * @param type
     *            Tipo terreno delle tessere.
     * @param height
     *            Estensione del mazzo che si vuole costruire.
     * @return Ritorna il mazzo creato.
     */
    protected ArrayList<TerrainCard> createOneTypeDeck(TerrainType type,
	    int height) {
	ArrayList<TerrainCard> deck = new ArrayList<TerrainCard>();

	for (int i = height; i >= 0; i--) {
	    deck.add(new TerrainCard(type, i));
	}

	return deck;
    }

    /**
     * Aggiunge un giocatore alla lista dei giocatori.
     * 
     * @param nickname
     *            Nome del giocatore scelto dall'utente.
     * @param gold
     *            Oro iniziale del giocatore.
     * @param card
     *            Tessera iniziale casuale del giocatore.
     */
    public void addPlayer(String id, String nickname, int gold, TerrainCard card) {
	this.players.add(new Player(id, nickname, gold, card));
    }

    public void addSheperdToPlayer(String idPlayer, String idShepherd, int gold) {
	for (Player player : players) {
	    if (player.getId().equals(idPlayer)) {
		player.addShepherd(new Shepherd(player, idShepherd, gold));
	    }
	}
    }

    /**
     * Trova il pastore in base al campo ID di Sheperd cercando tra gli
     * ArrayList di Sheperd del Player.
     * 
     * @param idPlayer
     *            ID del Player su cui effettuare la ricerca.
     * @param idSheperd
     *            ID dello Sheperd da trovare.
     * @return Oggetto di classe Sheperd trovato.
     */
    public Shepherd findSheperdFromID(String idPlayer, String idSheperd) {
	Player currentPlayer = players.get(Integer.parseInt(idPlayer));
	for (Shepherd sheperd : currentPlayer.getShepherds()) {
	    if (sheperd.getId().equals(idSheperd)) {
		return sheperd;
	    }
	}
	return null;
    }

    /**
     * Metodo usato dal Controller quando il giocatore sceglie la posizione per
     * il pastore.
     * 
     */
    public void setShepherdPosition(String idPlayer, String idShepherd,
	    String idRoad, String color) {
	setChanged();
	notifyObservers(new ViewAction(ViewAction.Command.SETPOSITION,
		idPlayer, idShepherd, idRoad, color));
    }

    /**
     * Metodo usato dal Controller quando il giocatore acquista una tessera.
     */
    public void buyCard(String idPlayer, String idShepherd, String cardType,
	    String cardPrice, String playersCardCount) {
	SheeplandAction arg = new SheeplandAction(SheeplandAction.Move.BUYCARD,
		cardType, cardPrice, playersCardCount);
	setChanged();
	notifyObservers(arg);
	int playerWallet = players.get(Integer.parseInt(idPlayer)).getGold();
	setChanged();
	notifyObservers(new SheeplandAction(SheeplandAction.Move.SPENDGOLD,
		idPlayer, Integer.toString(playerWallet)));
    }

    /**
     * Metodo usato dal Controller che rappresenta l'azione di spostamento del
     * pastore.
     */
    public void moveSheperd(String idPlayer, String idShepherd, String idRoad) {
	SheeplandAction action = new SheeplandAction(
		SheeplandAction.Move.MOVESHEPERD, idPlayer, idShepherd, idRoad,
		Integer.toString(fences));
	setChanged();
	notifyObservers(action);
	int playerWallet = players.get(Integer.parseInt(idPlayer)).getGold();
	setChanged();
	notifyObservers(new SheeplandAction(SheeplandAction.Move.SPENDGOLD,
		idPlayer, Integer.toString(playerWallet)));
    }

    /**
     * Metodo usato dal Controller che rappresenta l'azione di spostamento di un
     * animale da una regione.
     */
    public void moveAnimal(String idPlayer, String idShepherd, String animal,
	    String idOriginRegion, String idDestinationRegion) {
	SheeplandAction action = new SheeplandAction(
		SheeplandAction.Move.MOVEANIMAL, animal, idOriginRegion,
		idDestinationRegion);
	setChanged();
	notifyObservers(action);
    }

    /**
     * Restituisce un tipo di terreno casuale.
     * 
     * @return Tipo eliminato dall'ArrayList terrainTypes.
     */
    public TerrainType getRandomTerrainType() {
	Dice customDice = new Dice(terrainTypes.size());
	return terrainTypes.remove(customDice.roll() - 1);
    }

    public int getPlayersSize() {
	return players.size();
    }

    public Player getPlayer(String idPlayer) {
	return players.get(Integer.parseInt(idPlayer));
    }

    public Dice getDice() {
	return dice;
    }

    public ArrayList<Road> getRoads() {
	return roads;
    }

    public ArrayList<Region> getRegions() {
	return regions;
    }

    public HashMap<TerrainType, ArrayList<TerrainCard>> getDeckList() {
	return deckList;
    }

    public int getFences() {
	return fences;
    }

    public void removeFence() {
	fences--;
    }

    public void setReady(int i) {
	playersReady[i] = "v";
    }

    public void updatePlayersReady() {
    }

    public void updatePlayersReady(String id) {
	playersReady[1] = id;
	setChanged();
	notifyObservers(playersReady);// va al server
    }

    public void updatePlayers() {
    }

    public void updatePlayers(String id) {
	int c = 3;
	IDNickname = new String[players.size() * 2 + 3];
	IDNickname[0] = "IDNickname";
	IDNickname[1] = Integer.toString(players.size() * 2 + 3);
	IDAsker = id;
	IDNickname[2] = IDAsker;
	for (int i = 0; i < players.size(); i++) {
	    Player player = players.get(i);
	    IDNickname[c] = player.getId();
	    c++;
	    IDNickname[c] = player.getNickname();
	    c++;
	}

	setChanged();
	notifyObservers(IDNickname);
	System.out.println("notificato IDNickname al server dal suo model");

    }

    public void broadcastStart() {
    }

    /**
     * notifica a tutti i client collegati che qualcuno ha deciso di iniziare la
     * partita : (con un timeout sarebbe stato perfetto)
     */
    public void broadcastStart(String id) {
	broadcastString = new String[2];
	broadcastString[0] = "broadcastStart";
	broadcastString[1] = id;
	setChanged();
	notifyObservers(broadcastString);

    }

    public void broadcastReading() {

    }

	public void invioPlayersSize() {
		playersSize = new String[2];
		playersSize[0]="RICEZIONEPLAYERSSIZE";
		playersSize[1]=Integer.toString(players.size());
		setChanged();
		notifyObservers(playersSize);
	}

}
