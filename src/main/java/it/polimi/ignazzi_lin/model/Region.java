package it.polimi.ignazzi_lin.model;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * @author unil
 * 
 */
public class Region {

    private String id;

    private TerrainType type;

    ArrayList<Road> nearRoads;

    private String coordinates;

    /**
     * Hash di animali presenti nella regione. HashMap<Tipo Animale, Numero di
     * animali del tipo>
     */
    HashMap<AnimalType, Integer> animals = new HashMap<AnimalType, Integer>();

    /**
     * @param type
     *            Tipo della regione da creare.
     * @param firstAnimal
     *            Tipo dell'animale/i da inserire all'inizializzazione della
     *            regione.
     * @param animalCount
     *            Numero degli animali sopracitati.
     * @param coord
     */
    public Region(String id, TerrainType type, AnimalType firstAnimal,
	    int animalCount, String coordinates) {
	for (AnimalType animalType : AnimalType.values()) {
	    this.animals.put(animalType, 0);
	}
	this.id = id;
	this.type = type;
	this.animals.put(firstAnimal, animalCount);
	this.nearRoads = new ArrayList<Road>();
	this.coordinates = coordinates;
    }

    /**
     * Rimuove un animale dalla regione.
     * 
     * @param animal
     *            Tipo dell'animale da togliere.
     */
    private void removeAnimal(AnimalType animal) {
	if (animals.get(animal) > 0) {
	    animals.put(animal, animals.get(animal) - 1);
	}
    }

    /**
     * Aggiunge un animale alla regione.
     * 
     * @param animal
     *            Tipo dell'animale da aggiungere.
     */
    private void addAnimal(AnimalType animal) {
	animals.put(animal, animals.get(animal) + 1);
    }

    /**
     * @param road
     *            Strada da considerare per trovare la regione opposta.
     * @return Regione opposta, rispetto ad una certa strada, a this.
     */
    public Region getOppositeRegion(Road road) {
	for (Region region : road.getNearRegions()) {
	    if (region != this) {
		return region;
	    }
	}
	return this;
    }

    public void moveAnimal(AnimalType animal, Region destinationRegion) {
	this.removeAnimal(animal);
	destinationRegion.addAnimal(animal);
    }

    /**
     * Verifica se this contiene un certo tipo di animale.
     * 
     * @param animal
     *            Il tipo di animale considerato.
     */
    public boolean contains(AnimalType animal) {
	if (animals.get(animal) <= 0) {
	    return false;
	}
	return true;
    }

    public String getId() {
	return id;
    }

    public TerrainType getType() {
	return type;
    }

    public ArrayList<Road> getNearRoads() {
	return nearRoads;
    }

    public void setNearRoads(ArrayList<Road> nearRoads) {
	this.nearRoads = nearRoads;
    }

    public HashMap<AnimalType, Integer> getAnimals() {
	return animals;
    }

    public int getX() {
	return Integer.parseInt(coordinates.split("x")[0]);
    }

    public int getY() {
	return Integer.parseInt(coordinates.split("x")[1]);
    }

}