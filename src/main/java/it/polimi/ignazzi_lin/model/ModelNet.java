package it.polimi.ignazzi_lin.model;

import it.polimi.ignazzi_lin.controller.NetAction;
import it.polimi.ignazzi_lin.controller.SheeplandAction;
import it.polimi.ignazzi_lin.controller.SocketClient;
import it.polimi.ignazzi_lin.controller.ViewAction;
import it.polimi.ignazzi_lin.controller.ViewAction.Command;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

/*
 * Questa classe l'ho intesa come model di rete, ovvero se si sceglie il gioco online,
 * le informazioni sn mandate per aggiornare il model del server. che poi a sua volta aggiornerà quello di tutti i giocatori client!
 * */
public class ModelNet extends Model {

    private ArrayList<Player> players = new ArrayList<Player>();
    private SocketClient socketClient;
    private Integer myID;
    private String[] playersReady = new String[6];
    private String[] broadcastString = new String[2];
    private SheeplandAction arg;
    private String sizeOfPlayers;

    /** COSTRUTTORE Iniziale senza ip e port */
    public ModelNet(int deckDimension, int diceFaces, ArrayList<Road> roads,
	    ArrayList<Region> regions, ArrayList<TerrainType> terrainTypes) {
	super(deckDimension, diceFaces, roads, regions, terrainTypes);

	this.createDecks(deckDimension);
    }

    /** COSTRUTTORE */
    public ModelNet(int deckDimension, int diceFaces, ArrayList<Road> roads,
	    ArrayList<Region> regions, ArrayList<TerrainType> terrainTypes,
	    String ip, int port) {
	super(deckDimension, diceFaces, roads, regions, terrainTypes);
	this.createDecks(deckDimension);

	socketClient = new SocketClient(ip, port);
	myID = socketClient.getIDclient();

	System.out.println("myID:" + myID + "\n\n\n\n");
	setChanged();
	notifyObservers(myID);
	// System.out.println("\nMODEL NET HA RICEVUTO DAL CLIENT l'ID CORRENTE:"+IDcorrente+"\n");
	// setChanged();
	// notifyObservers(IDcorrente); //TODO non funziona questa notifica alla
	// view!
	// System.out.println("\nMODEL NET HA NOTIFICATO ALLA GUI (MA NON CI RIESCE!!!!):"+IDcorrente+"\n");
	// //TODO
	//
    }

    /** METODI DI RETE PER AZIONI */
    /**
     * ---------------------------------------------------------------------
     * 
     * @throws IOException
     */

    @Override
    public void buyCard(String idPlayer, String idSheperd, String cardType,
	    String cardPrice, String playersCardCount) {
	if (myID.equals(idPlayer)) {
	    socketClient.writeMossa(SheeplandAction.Move.BUYCARD);
	    socketClient.buyCardNet(idPlayer, idSheperd, cardType, cardPrice,
		    playersCardCount);
	}
	try {
	    arg = socketClient.recieveUpdate();
	    UpdateModelBuyCard(arg);
	    setChanged();
	    notifyObservers(arg); // notifica alla view TODO

	} catch (IOException e) {
	    e.printStackTrace();
	}
    }

    public void buyCard(String cardType) {
	socketClient.writeMossa(SheeplandAction.Move.BUYCARD);
	socketClient.buyCardNet(cardType);
	try {
	    arg = socketClient.recieveUpdate();
	    UpdateModelBuyCard(arg);
	    setChanged();
	    notifyObservers(arg); // notifica alla view TODO

	} catch (IOException e) {
	    e.printStackTrace();
	}
    }

    private void UpdateModelBuyCard(SheeplandAction arg) {
	ArrayList<String> args = arg.getArgs();
	String cardType = args.get(0);
    }

    @Override
    public void moveSheperd(String idPlayer, String idShepherd, String idRoad) {
	socketClient.writeMossa(SheeplandAction.Move.MOVEANIMAL);
	socketClient.moveSheperdNet(idPlayer, idShepherd, idRoad);
	try {
	    arg = socketClient.recieveUpdate();
	    UpdateModelMoveShepherd(arg);
	    setChanged();
	    notifyObservers(arg); // notifica alla view TODO
	} catch (IOException e) {
	    e.printStackTrace();
	}
    }

    public void moveSheperd(String idRoad) {
	socketClient.writeMossa(SheeplandAction.Move.MOVEANIMAL);
	socketClient.moveSheperdNet(idRoad);
	try {
	    arg = socketClient.recieveUpdate();
	    UpdateModelMoveShepherd(arg);
	    setChanged();
	    notifyObservers(arg); // notifica alla view TODO
	} catch (IOException e) {
	    e.printStackTrace();
	}
    }

    private void UpdateModelMoveShepherd(SheeplandAction arg) {
	ArrayList<String> args = arg.getArgs();
	String idRoad = args.get(0);
    }

    @Override
    public void moveAnimal(String idPlayer, String idShepherd, String animal,
	    String idOriginRegion, String idDestinationRegion) {
	socketClient.writeMossa(SheeplandAction.Move.MOVESHEPERD);
	socketClient.moveAnimalNet(idPlayer, idShepherd, animal,
		idOriginRegion, idDestinationRegion);
    }

    public void moveAnimal(String animal, String idOriginRegion) {
	socketClient.writeMossa(SheeplandAction.Move.MOVESHEPERD);
	socketClient.moveAnimalNet(animal, idOriginRegion);
    }

    private void UpdateModelMoveAnimal(SheeplandAction arg) {
	ArrayList<String> args = arg.getArgs();
	String animal = args.get(0);
	String idOriginRegion = args.get(1);

    }

    @Override
    public void addPlayer(String id, String nickname, int gold, TerrainCard card) {
	socketClient.writeString("addplayer");
	socketClient.addPlayerNet(id, nickname);
	// updatePlayers(); //TODO ORA SI FA QUESTO!
	/*
	 * try { argc = socketClient.recieveUpdateCommand();
	 * UpdateModelAddPlayer(argc); setChanged(); notifyObservers(players);
	 * // TODO } catch (IOException e) { e.printStackTrace(); }
	 */

    }

    /**
     * Modifica il Model dei client con i dati che arrivano dal SERVER.
     * (aggiunta giocatori)
     */
    private void UpdateModelAddPlayer(String[] argc) {
	System.out.println("Updating del model (addplayer)");
	// String comando =argc[0];
	String idMossa = Integer.toString(myID);// argc[1];
	String nickname = argc[2];
	int gold = Integer.parseInt(argc[3]);
	TerrainType type = TerrainType.parseInput(argc[4]);
	int price = Integer.parseInt(argc[5]);
	TerrainCard card = new TerrainCard(type, price);
	this.players.add(new Player(idMossa, nickname, gold, card));
	System.out.println("model Updated!");

    }

    @Override
    public void setShepherdPosition(String idPlayer, String idShepherd,
	    String idRoad, String color) {
	socketClient.writeString(ViewAction.Command.SETPOSITION.toString());
	socketClient
		.setShepherdPositionNet(idPlayer, idShepherd, idRoad, color);
    }

    @Override
    public void setReady(int i) {
	socketClient.writeString("posizione");
	socketClient.writeString(Integer.toString(i));
    }

    @Override
    public void updatePlayersReady() {
	try {
	    socketClient.writeString("updatePlayersReady");
	    playersReady = socketClient.recieveUpdateCommand();
	    setChanged();
	    notifyObservers(playersReady);
	} catch (IOException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}
    }

    /** dice al server di recuperare i dati da players */
    public void updatePlayers() {
	socketClient.writeString("updatePlayers");
	String[] IDNickname = new String[11];
	System.out
		.println("creato IDNickname locale in attesa di aggiornarlo con quello del server");

	IDNickname = socketClient.recievePlayersIDNickname();
	System.out.println("AGGIORNATO IDNickname");
	setChanged();
	notifyObservers(IDNickname); // va alla gui
	System.out.println("notificato IDNickname alla gui");

    }

    /** dice al server che è stato schiacciato un parti. */
    @Override
    public void broadcastStart() {
	try {
	    socketClient.writeString("broadcastStart");
	    System.out
		    .println("in attesa di aggiornare la stringa di start dal server.");

	    // // tolgo e lo metto dopo il pronto di tutti.
	    broadcastString[0] = socketClient.readingFromServer();
	    broadcastString[1] = socketClient.readingFromServer();

	    System.out.println("letto,next: notificare la propria gui.");

	    setChanged();
	    notifyObservers(broadcastString); // va alla gui
	    System.out.println("notificato alla gui il comando:"
		    + broadcastString[0]);

	} catch (IOException e) {
	    e.printStackTrace();
	}
    }
    
    public void startAction() {
	try {
		socketClient.bw.write("START");
		socketClient.bw.newLine();
		socketClient.bw.flush();
		socketClient.bw.write(getMyID());
		socketClient.bw.newLine();
		socketClient.bw.flush();
	    } catch (IOException e) {
		e.printStackTrace();
	    }
    }

    /** chi non effettua la mossa riceve il broadcast. */
    @Override
    public void broadcastReading() {
	try {
	    System.out.println("MI METTO IN ATTESA.\n");

	    broadcastString[0] = socketClient.readingFromServer();
	    broadcastString[1] = socketClient.readingFromServer();
	    System.out.println("letto,next: notificare la propria gui.");

	    setChanged();
	    notifyObservers(broadcastString); // va alla gui
	    System.out.println("notificato alla gui il comando:"
		    + broadcastString[0]);

	} catch (IOException e) {
	    e.printStackTrace();
	}
    }

    /**
     * Notifica alla gui la mossa di gioco.
     */
    public void movesWaiting() {
	SheeplandAction move;
	move = socketClient.recieveMoves();
	notifyObservers(move);
    }

    public String getMyID() {
	String id = Integer.toString(myID);
	return id;
    }

    public int getPlayersSizeFromServer() {
	try {
	    socketClient.writeNetAction(NetAction.Command.GETPLAYERSSIZE);
	    socketClient.bw.write(myID);
	    socketClient.bw.newLine();
	    socketClient.bw.flush();
	    sizeOfPlayers = socketClient.readingFromServer();
	    return Integer.parseInt(sizeOfPlayers);

	} catch (IOException e) {
	    e.printStackTrace();
	    return -1;
	}

    }

    @Override
    public void addSheperdToPlayer(String idRequest, String idSheperd, int gold) {
	try {
	    socketClient.writeNetAction(NetAction.Command.ADDSHEPHERD);
	    socketClient.bw.write(myID);

	} catch (IOException e) {
	    e.printStackTrace();

	}
  }
}
