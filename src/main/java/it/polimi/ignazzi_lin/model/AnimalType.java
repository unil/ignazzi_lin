/**
 * 
 */
package it.polimi.ignazzi_lin.model;

/**
 * Tipi di animali che possono trovarsi nelle regioni.
 * 
 * @author unil
 *
 */
public enum AnimalType {
    
    SHEEP, BLACKSHEEP, RAM, LAMB, WOLF;
    
    public static AnimalType parseInput(String input){
	return Enum.valueOf(AnimalType.class, input.toUpperCase());
    }

}
