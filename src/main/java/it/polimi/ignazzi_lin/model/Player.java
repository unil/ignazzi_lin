package it.polimi.ignazzi_lin.model;

import java.util.ArrayList;

public class Player {

    private String id;

    private String nickname;

    private int gold;

    private ArrayList<TerrainCard> cards = new ArrayList<TerrainCard>();

    private ArrayList<Shepherd> shepherds = new ArrayList<Shepherd>();

    private Color color;

    public enum Color {
	RED, BLUE, GREEN, YELLOW;

	public static Color parseInput(String input) {
	    return Enum.valueOf(Color.class, input.toUpperCase());
	}

	public static Color parseID(String id) {
	    switch (id) {
	    case "0":
		return RED;
	    case "1":
		return BLUE;
	    case "2":
		return GREEN;
	    case "3":
		return YELLOW;
	    default:
		return null;
	    }
	}
    }

    public Player(String id, String nickname, int gold, TerrainCard card) {
	this.id = id;
	this.nickname = nickname;
	this.addShepherd(new Shepherd(this, "0", gold));
	this.cards.add(card);
	this.color = Color.parseID(id);
    }

    public void addShepherd(Shepherd shepherd) {
	this.shepherds.add(shepherd);
    }

    public ArrayList<Shepherd> getShepherds() {
	return shepherds;
    }
    
    public Shepherd getShepherd(String idShepherd) {
	return shepherds.get(Integer.parseInt(idShepherd));
    }

    public String getId() {
	return id;
    }

    public String getNickname() {
	return nickname;
    }

    public void setGold(int gold) {
	this.gold = gold;
    }

    public int getGold() {
	return gold;
    }

    public ArrayList<TerrainCard> getCards() {
	return cards;
    }

    public Color getColor() {
	return color;
    }

}
