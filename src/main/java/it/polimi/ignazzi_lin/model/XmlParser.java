package it.polimi.ignazzi_lin.model;

import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * Classe che da un file xml carica le info necessarie al Model del gioco.
 * 
 * @author unil
 *
 */
public class XmlParser {

    private Document doc;

    private ArrayList<Road> roads;

    private ArrayList<Region> regions;

    private ArrayList<TerrainType> terrainTypes;

    private String mapImageName;

    public XmlParser() {
	try {
	    DocumentBuilderFactory dbFactory = DocumentBuilderFactory
		    .newInstance();

	    DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
	    this.doc = dBuilder.parse(getClass().getResourceAsStream("/Map.xml"));

	    this.roads = new ArrayList<Road>();
	    this.regions = new ArrayList<Region>();
	    this.terrainTypes = new ArrayList<TerrainType>();
	} catch (Exception e) {

	}
    }

    public ArrayList<Road> getRoads() {
	return roads;
    }

    public ArrayList<Region> getRegions() {
	return regions;
    }

    public String getMapImageName() {
	this.fetchElements("mapimage");
	return this.mapImageName;
    }

    public ArrayList<TerrainType> getTerrainTypes() {
	this.fetchElements("type");
	return this.terrainTypes;
    }

    /**
     * Carica tutta la mappa in base alle informazioni contenute nel documento
     * map.xml gli oggetti Road e Region vengono salvati nelle ArrayList della
     * classe.
     */
    public void loadMap() {
	this.fetchElements("region");
	this.fetchElements("road");
	this.loadRoadsVicinity();
	this.loadRegionsVicinity();
    }

    /**
     * @param tagName
     *            Nome del nodo contenente informazioni.
     */
    private void fetchElements(String tagName) {
	NodeList nodelist = this.doc.getElementsByTagName(tagName);
	for (int j = 0, l = nodelist.getLength(); j < l; ++j) {
	    Node innerNode = nodelist.item(j);
	    Element element = (Element) innerNode;
	    this.parseAttributes(element, tagName);
	}
    }

    /**
     * Prende delle informazioni in base ai parametri passati ed aggiunge nuovi
     * oggetti, Road o Region, ai rispettivi ArrayList della classe.
     * 
     * @param element
     *            Elemento da cui ottenere informazioni.
     * @param tagName
     *            Nome del nodo interessato nel documento xml.
     */
    private void parseAttributes(Element element, String tagName) {
	switch (tagName) {
	case "region":
	    String idRegion = element.getAttribute("id");
	    String type = element.getAttribute("type");
	    TerrainType terrainType = TerrainType.parseInput(type);
	    String coord = element.getAttribute("coords");
	    Region newRegion;
	    if (terrainType == TerrainType.SHEEPSBURG) {
		newRegion = new Region(idRegion, terrainType,
			AnimalType.BLACKSHEEP, Constants.SHEEPSNUMBER, coord);
	    } else {
		newRegion = new Region(idRegion, terrainType, AnimalType.SHEEP,
			Constants.SHEEPSNUMBER, coord);
	    }

	    this.regions.add(newRegion);
	    break;
	case "road":
	    String idRoad = element.getAttribute("id");
	    idRoad = Integer.toString(Integer.parseInt(idRoad) - 1);
	    Integer diceNumber = Integer.parseInt(element.getAttribute("dice"));
	    String coordinates = element.getAttribute("coords");
	    Road newRoad = new Road(idRoad, diceNumber, coordinates);
	    this.roads.add(newRoad);
	    break;
	case "mapimage":
	    mapImageName = element.getAttribute("name");
	    break;
	case "type":
	    String typeName = element.getAttribute("name");
	    TerrainType terraType = TerrainType.parseInput(typeName);
	    this.terrainTypes.add(terraType);
	    break;
	default:
	    System.err.println("Errore!");
	}
    }

    /**
     * Carica le adiacenze per le Strade in base alle informazioni contenute nel
     * file map.xml.
     */
    private void loadRoadsVicinity() {
	int i = 0;
	for (Road road : this.roads) {
	    ArrayList<Road> tempNearRoads = new ArrayList<Road>();
	    ArrayList<Region> tempNearRegions = new ArrayList<Region>();
	    NodeList nodeList = this.doc.getElementsByTagName("road");
	    Node innerNode = nodeList.item(i);

	    NodeList innerNodeList = ((Element) innerNode)
		    .getElementsByTagName("neighbor");
	    for (int j = 0, l = innerNodeList.getLength(); j < l; ++j) {
		Node roadInfo = innerNodeList.item(j);
		Element element = (Element) roadInfo;
		Road neighborRoad = this.roads.get(Integer.parseInt(element
			.getAttribute("id")) - 1);
		tempNearRoads.add(neighborRoad);
	    }

	    innerNodeList = ((Element) innerNode).getElementsByTagName("edge");
	    for (int j = 0, l = innerNodeList.getLength(); j < l; ++j) {
		Node roadInfo = innerNodeList.item(j);
		Element element = (Element) roadInfo;
		Region neighborRegion = this.regions.get(Integer
			.parseInt(element.getAttribute("id")));
		tempNearRegions.add(neighborRegion);
	    }

	    road.setNearRoads(tempNearRoads);
	    road.setNearRegions(tempNearRegions);

	    i++;
	}
    }

    /**
     * Carica le adiacenze per le Regioni, informazione non contenuta nel file
     * map.xml.
     */
    private void loadRegionsVicinity() {
	for (Region region : this.regions) {
	    int i = 0;
	    ArrayList<Road> tempNearRoads = new ArrayList<Road>();
	    for (Road road : this.roads) {
		if (road.getNearRegions().contains(region)) {
		    tempNearRoads.add(road);
		    i++;
		    if (i >= 6) {
			break;
		    }
		}
	    }
	    region.setNearRoads(tempNearRoads);
	}
    }

}
