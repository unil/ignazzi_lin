package it.polimi.ignazzi_lin.model;

/**
 * Enum che tiene i tipi che i terreni possono avere.
 * 
 * @author unil
 *
 */
public enum TerrainType {
    
    SHEEPSBURG, SPAWN, MOUNTAIN, FOREST, FIELD, DESERT, VALLEY;
    
    public static TerrainType parseInput(String input){
	return Enum.valueOf(TerrainType.class, input.toUpperCase());
    }
    
}