package it.polimi.ignazzi_lin.model;

/**
 * Classe con costanti del gioco.
 * 
 * @author unil
 *
 */
public class Constants {

    public static final int FENCE_NUMBER = 20;
    
    public static final int CARD_PRICE = 0;
    
    public static final int MOVINGPRICE = 1;
    
    public static final int MOVES = 3;

    public static final int PLAYER_GOLD = 20;

    public static final int TWO_PP_GOLD = 30;

    public static final int DICEFACES = 6;

    public static final int DECKDIMENSION = 4;
    
    public static final int BLACK_SHEEP_VALUE = 2;

    public final static int SHEEPSNUMBER = 1;
    
}
