package it.polimi.ignazzi_lin.model;

import java.util.Random;

/**
 * La classe implementa un dado di n facce.
 * 
 * @author unil
 *
 */
public class Dice {
    
    private int faces;
    
    public Dice(int faces) {
	this.faces = faces;
    }
	
    public int roll() {
	Random generator = new Random();
	return generator.nextInt(faces) + 1;		
    }
    
}