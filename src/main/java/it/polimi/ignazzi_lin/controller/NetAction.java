package it.polimi.ignazzi_lin.controller;

import java.util.ArrayList;

/**
 * Classe che contiene un Enum per identificare le azioni  attraverso la rete.
 * 
 * @author unil
 *
 */
public class NetAction {
    
    public enum Command {    
 	MESSAGE, NEXTPLAYER, JROADNAME, CARDANIMALS, SHOWGOLD, INITANIMALS, INITROADS,GETPLAYERSSIZE,ADDSHEPHERD;

 	public static Command parseInput(String input) {
 		return Enum.valueOf(Command.class, input.toUpperCase());
 	}
    }
    private Command command;
    
    private ArrayList<String> args = new ArrayList<String>();
    
    public NetAction(Command command, String... args) {
	this.command = command;
	for(int i = 0; i < args.length; i++) {
	    this.args.add(args[i]);
	}
    }
      
    public Command getCommand() {
        return this.command;
    }
    
    public ArrayList<String> getArgs() {
	return this.args;
    }
}
