package it.polimi.ignazzi_lin.controller;

import it.polimi.ignazzi_lin.model.AnimalType;
import it.polimi.ignazzi_lin.model.Constants;
import it.polimi.ignazzi_lin.model.Dice;
import it.polimi.ignazzi_lin.model.Model;
import it.polimi.ignazzi_lin.model.ModelNet;
import it.polimi.ignazzi_lin.model.Player;
import it.polimi.ignazzi_lin.model.Player.Color;
import it.polimi.ignazzi_lin.model.Region;
import it.polimi.ignazzi_lin.model.Road;
import it.polimi.ignazzi_lin.model.Shepherd;
import it.polimi.ignazzi_lin.model.TerrainCard;
import it.polimi.ignazzi_lin.model.TerrainType;
import it.polimi.ignazzi_lin.model.XmlParser;
import it.polimi.ignazzi_lin.view.View;

import java.io.BufferedWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Observable;
import java.util.Observer;

public class Controller implements Observer {

    private boolean boughtCard = false;

    private boolean movedAnimal = false;

    private Model model;

    private View view;

    private String currentPlayerID = Integer.toString(0);

    private String currentShepherdID = Integer.toString(0);

    private int playersNumber;

    private int shepherdsInOnePlayer;

    private boolean lastRound = false;

    private boolean choosingShepherd = false;

    private int moves = Constants.MOVES;

    private XmlParser xmlParser;

    public Controller(Model model, View view) {
	this.model = model;
	this.view = view;
    }

    @Override
    public void update(Observable o, Object arg) {
	if (o instanceof MultiSocketServer) {
	    view = (MultiSocketServer) o;
	}
	if (o != view
		|| (!(arg instanceof SheeplandAction)
			&& !(arg instanceof ViewAction)
			&& !(arg instanceof String)
			&& !(arg instanceof String[]) && !(arg instanceof BufferedWriter))) {
	    throw new IllegalArgumentException();
	}

	if (arg instanceof BufferedWriter) {
	} else {
	    if (arg instanceof SheeplandAction) {
		if (!choosingShepherd) {
		    if (isLegalmove((SheeplandAction) arg)) {
			manageTurn((SheeplandAction) arg);
		    }
		}
	    }
	    if (arg instanceof ViewAction) {
		viewActionReceived((ViewAction) arg);
	    }
	    if(arg instanceof NetAction) {
		netActionReceived((NetAction) arg);
	    }
	    if (arg instanceof String) {
		initModel((String) arg);
	    }

	    if (arg instanceof String[]) {
		String[] argcasted = (String[]) arg;

		switch (argcasted[0]) {

		case "broadcastStart":
		    model.broadcastStart(argcasted[1]);
		    break;

		case "stringReady": // aggiorna nel proprio modelNet l'array di
				    // stringhe playersReady;
		    for (int i = 0; i < 6; i++) {
			if (argcasted[i].equals("v")) {
			    model.setReady(i);

			}
		    }
		    break;
		case "posizione":
		    model.setReady(Integer.parseInt(argcasted[1]));
		    break;

		case "updateRequestPlayersReady": // dai client
		    model.updatePlayersReady(argcasted[1]);
		    break;
		case "updateRequestPlayers":
		    model.updatePlayers(argcasted[1]);
		    break;

		case "addplayerOnline":
		    String idOnlineVersion = argcasted[1];
		    String nicknameOnlineVersion = argcasted[2];
		    model.addPlayer(idOnlineVersion, nicknameOnlineVersion, Constants.PLAYER_GOLD,
			    new TerrainCard(model.getRandomTerrainType(), 0));

		    break;
		case "addplayer":
		    String idPlayer = argcasted[1];
		    String nickname = argcasted[2];
		    model.addPlayer(idPlayer, nickname, Constants.PLAYER_GOLD,
			    new TerrainCard(model.getRandomTerrainType(), 0));
		    break;

		default:
		    throw new IllegalArgumentException();
		}

	    }
	}
    }

    /**
     * Gestisce i comandi che invia l'utente.
     * 
     * @param arg
     *            Oggetto ViewAction inviato dalla view a seguito di una azione
     *            dell'utente.
     */
    private void viewActionReceived(ViewAction arg) {
	ViewAction.Command viewActionCommand = arg.getCommand();
	ArrayList<String> args = arg.getArgs();
	switch (viewActionCommand) {
	case ADDPLAYER:
	    String idPlayer = args.get(0);
	    String nickname = args.get(1);
	    model.addPlayer(idPlayer, nickname, Constants.PLAYER_GOLD,
		    new TerrainCard(model.getRandomTerrainType(), 0));
	    break;
	case CHOOSESHEP:
	    if (choosingShepherd) {
		String player = args.get(0);
		String shepherd = args.get(1);
		if (player.equals(currentPlayerID)) {
		    currentShepherdID = shepherd;
		    choosingShepherd = false;
		    view.showMessage("Pastore scelto!");
		} else {
		    view.showMessage("Clicca un tuo pastore!");
		}
	    }
	    break;
	case SETPOSITION:
	    if (currentShepherdID.equals(Integer
		    .toString(shepherdsInOnePlayer - 1))) {
		view.nextPlayer(currentPlayerID, nextPlayer());
	    }
	    String idRoad = args.get(0);

	    Shepherd currentShepherd = model.findSheperdFromID(currentPlayerID,
		    currentShepherdID);
	    Road position = model.getRoads().get(Integer.parseInt(idRoad));
	    if (!(position.isOccupied())) {
		currentShepherd.setPosition(position);
		Player currentPlayer = currentShepherd.getPlayerOwner();
		Color color = currentPlayer.getColor();
		model.setShepherdPosition(currentPlayerID, currentShepherdID,
			idRoad, color.toString());
	    } else {
		view.showMessage("Posizione già occupata!\nRiselezionare una posizione valida.");
		break;
	    }

	    boolean allShepherdsSet = currentPlayerID.equals(Integer
		    .toString(playersNumber - 1))
		    && currentShepherdID.equals(Integer
			    .toString(shepherdsInOnePlayer - 1));
	    if (allShepherdsSet) {
		view.setJRoadsName("jroad");
		view.addListenerCardsAnimals();
		if (playersNumber == 2) {
		    choosingShepherd = true;
		    view.showMessage("Selezionare il pastore che si desidera usare.");
		}
	    }
	    currentShepherdID = nextSheperd();
	    if (currentShepherdID.equals(Integer.toString(0))) {
		currentPlayerID = nextPlayer();
	    }
	    if (!allShepherdsSet) {
		showInitialCard();
		int playerCount = Integer.parseInt(currentPlayerID) + 1;
		int shepherdCount = Integer.parseInt(currentShepherdID) + 1;
		view.showMessage("Giocatore n " + playerCount
			+ "° scegli la posizione del pastore n "
			+ shepherdCount + "°.");
	    }
	    break;
	case START:
	    playersNumber = model.getPlayersSize();
	    shepherdsInOnePlayer = 1;
	    view.showGoldCounters(Integer.toString(Constants.PLAYER_GOLD));
	    if (playersNumber == 2) {
		shepherdsInOnePlayer = 2;
		for (int id = 0; id < playersNumber; id++) {
		    model.addSheperdToPlayer(Integer.toString(id),
			    Integer.toString(1), Constants.TWO_PP_GOLD);
		}

		view.showGoldCounters(Integer.toString(Constants.TWO_PP_GOLD));
	    }

	    ArrayList<Road> roads = (ArrayList<Road>) model.getRoads().clone();
	    ArrayList<Integer> allXroad = new ArrayList<Integer>();
	    ArrayList<Integer> allYroad = new ArrayList<Integer>();
	    ArrayList<String> allIDsroad = new ArrayList<String>();
	    for (Road road : roads) {
		allXroad.add(road.getX());
		allYroad.add(road.getY());
		allIDsroad.add(road.getId());
	    }
	    view.initializeJRoads(allXroad, allYroad, allIDsroad);

	    ArrayList<Region> regions = (ArrayList<Region>) model.getRegions()
		    .clone();
	    ArrayList<Integer> allXregion = new ArrayList<Integer>();
	    ArrayList<Integer> allYregion = new ArrayList<Integer>();
	    ArrayList<String> allIDsregion = new ArrayList<String>();
	    for (Region region : regions) {
		allXregion.add(region.getX());
		allYregion.add(region.getY());
		allIDsregion.add(region.getId());
	    }
	    view.initializeJAnimals(allXregion, allYregion, allIDsregion);

	    int playerCount = Integer.parseInt(currentPlayerID) + 1;
	    int shepherdCount = Integer.parseInt(currentShepherdID) + 1;

	    showInitialCard();

	    view.showMessage("Giocatore n " + playerCount
		    + "° scegli la posizione del pastore n " + shepherdCount
		    + "°.");
	    view.nextPlayer(currentPlayerID, currentPlayerID);

	    break;
	case RUNSERVER:
	    view.deleteObserver(this);
	    model.deleteObserver(view);
	    this.view = new MultiSocketServer(this, model);
	    break;
	case RUNCLIENT:
	    String ipRegex = "\\b\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\b:\\d{1,8}\\b";
	    String ip = args.get(0);
	    int portNumber = 0;
	    if (ip.matches(ipRegex)) {
		String[] a = ip.split(":");
		String ipString = a[0];
		String port = a[1];
		try {
		    portNumber = Integer.parseInt(port);
		} catch (Exception e) {
		    e.printStackTrace();
		}

		xmlParser = new XmlParser();
		xmlParser.loadMap();
		model.deleteObserver(view);
		view.deleteObserver(this);

		new ControllerClient(
			new ModelNet(Constants.DECKDIMENSION,
				Constants.DICEFACES, xmlParser.getRoads(),
				xmlParser.getRegions(),
				xmlParser.getTerrainTypes(), ipString,
				portNumber), view);
	    }
	    break;
	default:
	    throw new IllegalArgumentException();
	}
    }
    

    private void netActionReceived(NetAction arg) {
	NetAction.Command netActionCommand = arg.getCommand();
	ArrayList<String> args = arg.getArgs();
	switch (netActionCommand) {
	case ADDSHEPHERD:
	    String idPlayer = args.get(0);
	    String idShepherd = args.get(1);
	    int gold = Integer.parseInt(args.get(2));
	    model.addSheperdToPlayer(idPlayer, idShepherd, gold);
	    break;
	default:
	    throw new IllegalArgumentException();
	}	
    }

    private void showInitialCard() {
	TerrainCard card = model.getPlayer(currentPlayerID).getCards().get(0);
	String cardType = card.getType().toString();
	String cardPrice = Integer.toString(card.getPrice());
	model.buyCard(currentPlayerID, currentShepherdID, cardType, cardPrice,
		"1");
	showCards();
	view.JOptionPane("Hai ricevuto una tessera di tipo: " + cardType);
    }

    private void showCards() {
	for (TerrainType type : TerrainType.values()) {
	    if (type != TerrainType.SHEEPSBURG) {
		int playersCardCount = 0;
		ArrayList<TerrainCard> cardsOfType = model.getDeckList().get(
			type);
		String cardPrice = Integer.toString((cardsOfType
			.get(cardsOfType.size() - 1).getPrice()));
		for (TerrainCard card : model.getPlayer(currentPlayerID)
			.getCards()) {
		    if (card.getType().equals(type))
			playersCardCount++;
		}
		model.buyCard(currentPlayerID, currentShepherdID,
			type.toString(), cardPrice,
			Integer.toString(playersCardCount));
	    }
	}
    }

    /**
     * Controlla se l'azione compiuta dal giocatore può tradursi in una mossa
     * legale.
     * 
     * @param action
     *            Azione compiuta dal giocatore di tipo SheeplandAction.
     * @return true se la mossa è legale, false altrimenti.
     */
    private boolean isLegalmove(SheeplandAction action) {
	ArrayList<String> args = action.getArgs();
	Road shepherdPosition = model.getPlayer(currentPlayerID)
		.getShepherd(currentShepherdID).getPosition();
	switch (action.getMove()) {
	case BUYCARD:
	    TerrainType terrainType = TerrainType.parseInput(args.get(0));
	    ArrayList<TerrainType> typesInNearRegions = new ArrayList<TerrainType>();
	    for (Region region : shepherdPosition.getNearRegions()) {
		typesInNearRegions.add(region.getType());
	    }
	    ArrayList<TerrainCard> deckOfType = model.getDeckList().get(
		    terrainType);
	    boolean playerNoGoldForBuy = model.getPlayer(currentPlayerID)
		    .getGold()
		    - deckOfType.get(deckOfType.size() - 1).getPrice() < 0;
	    if (boughtCard || !(typesInNearRegions.contains(terrainType))
		    || model.getDeckList().get(terrainType).size() == 0
		    || playerNoGoldForBuy) {
		view.showMessage("Mossa non consentita!");
		return false;
	    }
	    return true;
	case MOVESHEPERD:
	    String roadToMoveIn = action.getArgs().get(0);
	    Road destination = model.getRoads().get(
		    Integer.parseInt(roadToMoveIn));
	    boolean playerNoGoldForTP = !(shepherdPosition.getNearRoads()
		    .contains(destination))
		    && model.getPlayer(currentPlayerID).getGold() < Constants.MOVINGPRICE;
	    if (destination.isOccupied() || playerNoGoldForTP) {
		view.showMessage("Mossa non consentita!");
		return false;
	    }
	    return true;
	case MOVEANIMAL:
	    String idOriginRegion = args.get(1);
	    Region originRegion = model.getRegions().get(
		    Integer.parseInt(idOriginRegion));
	    if (movedAnimal
		    || !(shepherdPosition.getNearRegions()
			    .contains(originRegion))) {
		view.showMessage("Mossa non consentita!");
		return false;
	    }
	    return true;
	default:
	    throw new IllegalArgumentException();
	}
    }

    /**
     * Gestisce le mosse del pastore all'interno del contesto del turno.
     * 
     * @param arg
     *            Tipo d'azione da far eseguire al pastore.
     */

    private void manageTurn(SheeplandAction arg) {
	if (this.moves > 0) {
	    this.moves--;
	    this.setPlayerMove(arg);
	    if (model.getFences() == 0 && !lastRound) {
		lastRound = true;
	    }
	    if (moves == 0) {
		if (model.getFences() == 0) {
		    if (lastRound
			    && Integer.parseInt(currentPlayerID) + 1 == playersNumber) {
			endGame();
			return;
		    }
		}
		moveRandomAnimal(AnimalType.BLACKSHEEP);
		view.nextPlayer(currentPlayerID, nextPlayer());
		this.moves = Constants.MOVES;
		currentShepherdID = nextSheperd();
		currentPlayerID = nextPlayer();
		if (playersNumber == 2) {
		    choosingShepherd = true;
		    view.showMessage("Selezionare il pastore che si desidera usare.");
		}
		showCards();
		this.boughtCard = false;
		this.movedAnimal = false;
	    }
	}
    }

    /**
     * Muove un'animale casualmente secondo un dado d6 (Dice).
     * 
     * @param animal
     *            Tipo dell'animale.
     */
    private void moveRandomAnimal(AnimalType animal) {
	int rollResult = new Dice(6).roll();
	view.JOptionPane("Dado della pecora nera: " + rollResult);
	Region originRegion = null;
	Region destinationRegion = null;
	for (Region region : model.getRegions()) {
	    if (region.contains(animal)) {
		for (Road nearRoad : region.getNearRoads()) {
		    if (rollResult == nearRoad.getDiceNumber()
			    && !(nearRoad.isOccupied())) {
			destinationRegion = region.getOppositeRegion(nearRoad);
			region.moveAnimal(animal, destinationRegion);
			break;
		    }
		}
		originRegion = region;
		break;
	    }
	}
	if (originRegion != null && destinationRegion != null) {
	    String idOriginRegion = originRegion.getId();
	    String idDestinationRegion = destinationRegion.getId();
	    model.moveAnimal(currentPlayerID, currentShepherdID,
		    animal.toString(), idOriginRegion, idDestinationRegion);
	}
    }

    /**
     * Calcola, al termine della partita, i punteggi finali dei giocatori.
     */
    private void endGame() {
	String message = "";
	for (int idPlayer = 0; idPlayer < playersNumber; idPlayer++) {
	    Player player = model.getPlayer(Integer.toString(idPlayer));
	    String playerName = player.getNickname();
	    int playerGold = player.getGold();
	    int animalsScore = 0;
	    int totalScore = 0;
	    for (TerrainCard card : player.getCards()) {
		TerrainType type = card.getType();
		for (Region region : model.getRegions()) {
		    if (region.getType().equals(type)) {
			HashMap<AnimalType, Integer> animals = region
				.getAnimals();
			int sheepScore = animals.get(AnimalType.SHEEP);
			int blackSheepScore = animals
				.get(AnimalType.BLACKSHEEP)
				* Constants.BLACK_SHEEP_VALUE;
			animalsScore += sheepScore + blackSheepScore;
		    }
		}
	    }
	    totalScore = playerGold + animalsScore;
	    message += ("Il giocatore \"" + playerName + "\" ha totalizzato "
		    + totalScore + " punti.\n");
	}
	view.JOptionPane(message);
    }

    /**
     * Gestisce le mosse possibili del pastore.
     * 
     * @param action
     *            Tipo di azione compiuta dal pastore.
     */
    private void setPlayerMove(SheeplandAction action) {
	Shepherd currentShepherd = model.findSheperdFromID(currentPlayerID,
		currentShepherdID);
	switch (action.getMove()) {
	case BUYCARD:
	    String cardType = action.getArgs().get(0);
	    TerrainType type = TerrainType.parseInput(cardType);
	    currentShepherd.buyCard(type, model.getDeckList());
	    ArrayList<TerrainCard> cardsOfType = model.getDeckList().get(type);
	    int cardPrice = cardsOfType.get(cardsOfType.size() - 1).getPrice();
	    int playersCardCount = 0;
	    for (TerrainCard card : currentShepherd.getPlayerOwner().getCards()) {
		if (card.getType().equals(type)) {
		    playersCardCount++;
		}
	    }
	    model.buyCard(currentPlayerID, currentShepherdID, cardType,
		    Integer.toString(cardPrice),
		    Integer.toString(playersCardCount));
	    this.boughtCard = true;
	    break;
	case MOVESHEPERD:
	    String roadToMoveIn = action.getArgs().get(0);
	    Road road = model.getRoads().get(Integer.parseInt(roadToMoveIn));
	    currentShepherd.moveTo(road);
	    if (model.getFences() > 0) {
		model.removeFence();
	    }
	    model.moveSheperd(currentPlayerID, currentShepherdID, roadToMoveIn);
	    this.boughtCard = false;
	    this.movedAnimal = false;
	    break;
	case MOVEANIMAL:
	    String animal = action.getArgs().get(0);
	    String idOriginRegion = action.getArgs().get(1);
	    Region originRegion = model.getRegions().get(
		    Integer.parseInt(idOriginRegion));
	    AnimalType animalType = AnimalType.parseInput(animal);
	    Road shepherdPosition = currentShepherd.getPosition();
	    Region destinationRegion = shepherdPosition.getNearRegions().get(0);
	    if (destinationRegion == originRegion) {
		destinationRegion = shepherdPosition.getNearRegions().get(1);
	    }
	    String idDestinationRegion = destinationRegion.getId();
	    currentShepherd.moveSheep(animalType, originRegion,
		    destinationRegion);
	    model.moveAnimal(currentPlayerID, currentShepherdID, animal,
		    idOriginRegion, idDestinationRegion);
	    this.movedAnimal = true;
	    break;
	default:
	    throw new IllegalArgumentException();
	}
    }

    private void initModel(String arg) {
	switch (arg) {
	case"GETPLAYERSSIZE":
		playersNumber = model.getPlayersSize();

		break;
	case"GETPLAYERSSTRING":
		model.invioPlayersSize();
		break;
	case "waiting":
	    model.broadcastReading();
	    break;

	case "broadcastStart":
	    model.broadcastStart();
	    break;

	case "updateRequestPlayersReady":
	    model.updatePlayersReady();
	    break;
	case "updateRequestPlayers":
	    model.updatePlayers();
	    break;
	default:
	    throw new IllegalArgumentException();
	}
    }

    /**
     * Trova l'ID del giocatore successivo a quello corrente e lo ritorna.
     */
    private String nextPlayer() {
	return (Integer.parseInt(currentPlayerID) == model.getPlayersSize() - 1) ? Integer
		.toString(0) : Integer.toString(Integer
		.parseInt(currentPlayerID) + 1);
    }

    /**
     * Trova l'ID del pastore successivo a quello corrente e lo ritorna.
     */
    private String nextSheperd() {
	ArrayList<Shepherd> shepherds = model.getPlayer(currentPlayerID)
		.getShepherds();

	if (Integer.parseInt(currentShepherdID) == shepherds.size() - 1) {
	    return model.getPlayer(nextPlayer()).getShepherds().get(0).getId();
	} else {
	    return Integer.toString(Integer.parseInt(currentShepherdID) + 1);

	}
    }

}
