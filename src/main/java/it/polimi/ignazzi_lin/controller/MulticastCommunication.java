package it.polimi.ignazzi_lin.controller;

import java.io.BufferedWriter;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

public class MulticastCommunication  implements Observer{

	private ArrayList<BufferedWriter> listaWriter = new ArrayList<BufferedWriter>();
	
	/**Costruttore*/
	public MulticastCommunication( ArrayList<BufferedWriter> listaWriter) {
		this.listaWriter = listaWriter;
	}
	
	@Override
	public void update(Observable o, Object arg) {
		// TODO Auto-generated method stub
		if(!(o instanceof MultiSocketServer)){
			throw new IllegalArgumentException();
		}
		if(arg instanceof BufferedWriter){
		System.out.println("dentro Update di mcc Aggiornamento ListaWriter");
		listaWriter.add((BufferedWriter) arg);
		}
	}
	

	public ArrayList<BufferedWriter> getListaBufferedWriter(){
		return listaWriter;
	}
	


}