package it.polimi.ignazzi_lin.controller;

import java.util.ArrayList;

/**
 * Enum che tiene conto delle azioni effettuate sulla GUI.
 * 
 * @author unil
 *
 */
public class ViewAction {

    public enum Command {
	SETPOSITION, ADDPLAYER, START, RUNCLIENT, CHOOSESHEP, RUNSERVER, GETPLAYERSSIZE, ADDSHEPHERD;

	public static Command parseInput(String input) {
	    return Enum.valueOf(Command.class, input.toUpperCase());
	}
    }

    private Command command;

    private ArrayList<String> args = new ArrayList<String>();

    public ViewAction(Command command, String... args) {
	this.command = command;
	for (int i = 0; i < args.length; i++) {
	    this.args.add(args[i]);
	}
    }

    public Command getCommand() {
	return this.command;
    }

    public ArrayList<String> getArgs() {
	return this.args;
    }
}
