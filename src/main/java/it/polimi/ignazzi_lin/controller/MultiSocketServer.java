package it.polimi.ignazzi_lin.controller;

import it.polimi.ignazzi_lin.controller.NetAction.Command;
import it.polimi.ignazzi_lin.controller.SheeplandAction.Move;
import it.polimi.ignazzi_lin.model.Model;
import it.polimi.ignazzi_lin.view.View;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Observable;

/**
 * La classe MultiSocketServer si mette in attesa di richieste da parte di 1 o
 * più Client. data la natura di TEST di questo codice, il computer locale
 * svolgerà sia il ruolo di client sia il ruolo di server.
 * 
 * @IMPORTANTE: l'istanza di MultiSocketServer deve chiamare addObserver di
 *              MulticastComunication e l'istanza di MultisocketServer invece
 *              deve chiamare addObserver sull'istanza del Controller
 * @author Ignazzi Marco.
 */

public class MultiSocketServer extends View implements Runnable {

	protected final static int PORT = 19999;
	public Socket connection1;
	private BufferedWriter bw, bwl;
	private BufferedReader br;
	private String TimeStamp;
	private int ID;
	private String readingString;
	private static ServerSocket serverSocket1;
	private MulticastCommunication mcc;// = new MulticastComunication();
	private String[] PlayersReady = new String[6];
	public static String dato2, dato3, dato4, dato5;
	private static ArrayList<BufferedWriter> listaWriter;
	private Controller controller;
	private Model model;
	private String[] posizione;
	private String[] requestPlayers = new String[2];
	private String[] requestPlayersReady = new String[2];
	private String[] broadcastString = new String[2];
	private String idRequestString;

	private int idRequest;

	/**
	 * COSTRUTTORE: SERVER MULTITHREAD -> resta in attesa e crea un nuovo thread
	 * per ogni client che si connette.
	 */
	public MultiSocketServer(Controller controller, Model model) {
		int count = 0;
		try {
			this.controller = controller;
			this.model = model;
			serverSocket1 = new ServerSocket(PORT);
			System.out.println("-----------------------------------------");
			System.out.println("MultiSocketServer Inizializzato.");
			System.out.println("In attesa di richieste da client..");
			System.out.println("-----------------------------------------\n");

			// attenzione ai thread che muoiono.
			while (count < 4) {
				Socket connection1 = serverSocket1.accept();
				Runnable runnable = new MultiSocketServer(connection1, count,
						controller, model);
				count++;
				Thread thread = new Thread(runnable);
				thread.start();
			}
		} catch (Exception e) {

		}
	}

	/**
	 * COSTRUTTORE CON PARAMETRI : socket e int per il conteggio dei client
	 * connessi
	 */
	public MultiSocketServer(Socket socket, int i, Controller controller,
			Model model) {
		this.connection1 = socket;
		this.ID = i;
		this.controller = controller;
		this.model = model;
	}

	/** METODO DI LETTURA DELL'INPUT STREAM */
	public String readingFromClient() throws IOException {
		System.out.println("\nNumero client riferimento: " + ID
				+ " - attendo che il buffer sia pronto per la lettura..");
		do {
		} while (!br.ready());

		String reading = br.readLine();
		System.out.println("-----------------------------------------");
		System.out.println("Contattato da client:\t" + ID);
		System.out.println("Ho ricevuto:\t" + reading);
		System.out.println("-----------------------------------------");
		return reading;
	} // chiude readingFromClient();

	/**
	 * METODO DI SCRITTURA VERSO IL CLIENT, invocato per verificare la
	 * connessione. (Aknowledge)
	 */
	public void writingConnect(BufferedWriter bw) throws IOException {
		TimeStamp = new java.util.Date().toString();
		String returnCode = "Il MultiSocketServer ha risposto all'host numero:"
				+ ID + " in data: " + TimeStamp;
		bw.write(returnCode);
		bw.newLine();
		bw.flush();
	}

	/** METODO RUN : CHIAMATO ALL'INIZIO DI OGNI NUOVO THREAD. */
	public void run() {
		try {
			dato3 = new String();
			dato4 = new String();
			dato5 = new String();

			String[] argAddPlayer = new String[6];
			listaWriter = new ArrayList<BufferedWriter>();
			mcc = new MulticastCommunication(listaWriter);
			this.addObserver(mcc);
			this.addObserver(controller);
			model.addObserver(this);

			/** INIZIALIZZAZIONE INPUT/OUTPUT STREAM SUL THREAD */
			/**
			 * ----------------------------------------------------------------
			 * -----------------------
			 */
			BufferedInputStream bis = new BufferedInputStream(
					connection1.getInputStream());
			InputStreamReader isr = new InputStreamReader(bis);
			br = new BufferedReader(isr);
			/**
			 * ----------------------------------------------------------------
			 * -----------------------
			 */
			BufferedOutputStream os = new BufferedOutputStream(
					connection1.getOutputStream());
			OutputStreamWriter osw = new OutputStreamWriter(os, "US-ASCII");
			bw = new BufferedWriter(osw);
			setChanged();
			notifyObservers(bw);
			/**
			 * ----------------------------------------------------------------
			 * -----------------------
			 */

			/*
			 * Completa l'inizializzazione, della connessione tra server e
			 * client
			 */

			readingFromClient();
			writingConnect(bw);
			System.out.println("\nOK!\tCONNESSIONE TESTATA CON CLIENT NUMERO:"
					+ ID + "\n");
			String idCorrente = Integer.toString(ID);
			bw.write(idCorrente);
			bw.newLine();
			bw.flush();
			System.out.println("Invio ID al client:" + ID + "\n\n");

			/* Inizio operazioni di comunicazione */

			/*
			 * data la natura di TEST di questo codice, faccio aspettare X
			 * secondi dopo ogni connessione,prima di rispondere. in questo modo
			 * si riesce a notare l'effettivo funzionamento del "MultiSocket"
			 * ovvero della programmazione concorrenziale implementata.
			 */
			/*
			 * try { Thread.sleep(500); } catch (Exception e) {}
			 */

			/**
			 * CICLO LETTURA SCRITTURA VERSO CLIENT X'esimo dato 1 e dato 2 per
			 * ora sono inutili, forse verranno aggiunti controlli sui
			 * giocatori, altrimenti verranno rimossi.
			 */

			do {
				System.out.println("\nINIZIO CICLO DI LETTURA SERVER!!!");

				System.out.println("\n------------------------------------");

				readingString = readingFromClient(); // lettura mossa || comando
				if (!readingString.equals(null)) {

					switch (readingString) {
					case "addplayer":
						argAddPlayer[0] = "addplayerOnline";
						argAddPlayer[1] = readingFromClient();//idClient
						argAddPlayer[2] = readingFromClient();//nickname
						setChanged();
						notifyObservers(argAddPlayer);
						break;
					// System.out.println("multicastWriteString CALL INCOMING...");
					// multicastWriteString("addplayer");
					// System.out.println("multicastWriteString compiuta.");
					case "updatePlayers":
						System.out.println("dentro case updatePlayers");
						requestPlayers[0] = "updateRequestPlayers";
						requestPlayers[1] = idCorrente;

						setChanged();
						notifyObservers(requestPlayers);
						System.out
								.println("Notificato updateRequestPlayers DAL SERVER");
						break;

					case "stringReady":
						PlayersReady[0] = "stringReady";
						PlayersReady[1] = readingFromClient();
						PlayersReady[2] = readingFromClient();
						PlayersReady[3] = readingFromClient();
						PlayersReady[4] = readingFromClient();
						PlayersReady[5] = readingFromClient();

						setChanged();
						notifyObservers(PlayersReady);
						break;
					case "posizione":
						posizione = new String[2];
						posizione[0] = "posizione";
						posizione[1] = readingFromClient();
						setChanged();
						notifyObservers(posizione);
						break;
					case "updatePlayersReady":
						System.out.println("dentro updatePlayersReady");
						requestPlayersReady[0] = "updateRequestPlayersReady";
						requestPlayersReady[1] = idCorrente;
						setChanged();
						notifyObservers(requestPlayersReady);
						break;
					case "broadcastStart":
						System.out.println("dentro broadcastStart");
						broadcastString[0] = "broadcastStart";
						broadcastString[1] = idCorrente;
						System.out.println("dentro broadcastStart0:"
								+ broadcastString[0]);
						System.out.println("dentro broadcastStart1"
								+ broadcastString[1]);

						setChanged();
						notifyObservers(broadcastString);
						break;

					case "quit": // TODO
						setChanged();
						notifyObservers((String) "playerGone");
						break;
					case "BUYCARD":
						dato3 = readingFromClient();// card type

						System.out.println("\nDATI BUYCARD RICEVUTI:" + dato3);

						SheeplandAction argCard = new SheeplandAction(
								SheeplandAction.Move.BUYCARD, dato3);
						setChanged();

						System.out
								.println("\nsto notificando mossa: compra carta");
						notifyObservers(argCard);
						System.out.println("\nnotificata mossa: compra carta");
						break;
					case "MOVESHEPHERD":
						dato3 = readingFromClient(); // id road

						System.out.println("\nDATI MOVESHEPHERD RICEVUTI:"
								+ dato3);

						SheeplandAction argPlayer = new SheeplandAction(
								SheeplandAction.Move.MOVESHEPERD, dato3);

						setChanged();

						System.out.println("\nsto notificando mossa pastore");
						notifyObservers(argPlayer);
						System.out.println("\nnotificata mossa pastore");

						break;
					case "MOVEANIMAL":
						dato3 = readingFromClient(); // animal
						dato4 = readingFromClient(); // id region

						System.out.println("\nDATI MOVE ANIMAL RICEVUTI:"
								+ dato3 + dato4);

						SheeplandAction argAnimal = new SheeplandAction(
								SheeplandAction.Move.MOVEANIMAL, dato3, dato4);
						setChanged();
						notifyObservers(argAnimal);
						System.out.println("\nnotificata mossa animale");

						break;
					case "GETPLAYERSSIZE":
						idRequestString=readingFromClient();
						idRequest= Integer.parseInt(idRequestString);
						setChanged();
						notifyObservers("GETPLAYERSSIZE");
						break;
					case "ADDSHEPHERD":
						setChanged();
						notifyObservers("ADDSHEPHERD");
						break;
					case "START":
						idRequestString= readingFromClient();
						idRequest= Integer.parseInt(idRequestString);
						ViewAction start = new ViewAction(ViewAction.Command.START , Integer.toString(idRequest) );
						setChanged();
						notifyObservers(start);
					default:
						System.out
								.println("arrivata al server una mossa illecita");
						break;
					}
				} else {
					System.out
							.println("Ho ricevuto un null... il client potrebbe essersi disconnesso! � cosi ?");
					// GESIONE DISCONNESSIONI DA AGGIUNGERE QUI
				}

			} while (!readingString.equals("quit")); // (!readingString.equals("quit")||readingString.equals("Quit")||readingString.equals("QUIT")));
			System.out
					.println("Ricevuto quit\t-->\til thread relativo all'host num: "
							+ ID + " muore\n");

			/*
			 * PER CHIUDERE IL SERVER
			 * System.out.println("Shutting down the server...");
			 * serverSocket1.close(); System.out.println("server closed");
			 */

		} catch (Exception e) {
			e.printStackTrace();

		} finally {
			try {
				System.out.println("... chiusura socket");
				connection1.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

	}

	/** Metodo di update dal model. */
	@Override
	public void update(Observable o, Object arg) {
		System.out.println("Dentro update del Server\n");

		if (!(o instanceof Model)) { // ??? arrivano da se stessa, si può ???
			throw new IllegalArgumentException();
		} else {
			if (arg instanceof SheeplandAction){
				try {
				String[] stringToClient = new String[4];
				SheeplandAction moveFromModel=(SheeplandAction)arg;
				stringToClient[0]=moveFromModel.getMove().toString();
				bw.write(stringToClient[0]);
				bw.newLine();
				bw.flush();
				stringToClient[1]=moveFromModel.getArgs().get(0);
				bw.write(stringToClient[1]);
				bw.newLine();
				bw.flush();
				if(stringToClient[0].equals(SheeplandAction.Move.MOVEANIMAL)){
					stringToClient[2]=moveFromModel.getArgs().get(1);
					bw.write(stringToClient[2]);
					bw.newLine();
					bw.flush();
				}
				
				
				}catch(IOException e){
				 e.printStackTrace();
				
			
				}
			}
			if (arg instanceof String[]) {
				try {
					String[] argcasted = new String[6];
					argcasted = (String[]) arg;
					String[] argcasted2 = new String[11];
					argcasted2 = (String[]) arg;
					System.out.println("argcasted[0] è :" + argcasted[0]);

					switch (argcasted[0]) {
					
					case "RICEZIONEPLAYERSSIZE":
						if(ID==idRequest){
							bw.write(argcasted2[1]);
							bw.newLine();
							bw.newLine();
						}
						else{
							System.out.println("ignorato l'invio al client numero:" + ID);
						}
						break;
			
						
					case "broadcastStart":
						System.out.println("invio broadcast da: "
								+ argcasted[1] + " a: " + ID);

						bw.write(argcasted[0]);
						bw.newLine();
						bw.flush();
						System.out
								.println("è stato inviato--->" + argcasted[0]);

						bw.write(argcasted[1]);
						bw.newLine();
						bw.flush();
						System.out
								.println("è stato inviato--->" + argcasted[1]);

						break;
					case "stringReady": // aggiornamento su richiesta della
										// playersReady.
						System.out.println("dentro UPDATE case:stringReady");
						if (ID == Integer.parseInt(argcasted[1])) {

							for (int i = 0; i < 6; i++) {
								bw.write(argcasted[i]);
								bw.newLine();
								bw.flush();
								System.out.println("\n Scritto al client---->"
										+ argcasted[i]);

							}

							System.out.println("\n-----------------------");

							break;
						} else {
							System.out
									.println("messaggio non inviato agli altri ID:"
											+ ID);
							break;
						}
					case "IDNickname":
						System.out.println("dentro UPDATE IDNickname");
						if (ID == Integer.parseInt(argcasted[2])) {

							int size = Integer.parseInt(argcasted2[1]);

							for (int i = 0; i < size; i++) {
								bw.write(argcasted2[i]);
								bw.newLine();
								bw.flush();
								System.out.println("\n Scritto al client---->"
										+ argcasted2[i]);
							}
							System.out.println("\n-----------------------");

							break;
						} else {
							System.out
									.println("messaggio non inviato agli altri ID:"
											+ ID);
						}
						break;
					default:
						throw new IllegalArgumentException();

					}

				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}

	/**
	 * METODO CHE MANDA A TUTTI I GIOCATORI( SUI LORO STREAM ) LA MOSSA E I DATI
	 * CHE SERVONO PER ADEMPIERLA. PS: sarebbe più facile mandarla a tutti
	 * tranne il giocatore che compie la mossa. in questo modo si evita
	 * facilmente il problema della DOPPIA MOSSA (in questo momento si notifica
	 * il controller anche quando mando l'azione.)
	 */
	public void multicastWrite(Object arg) throws IOException {
		SheeplandAction.Move mossa = ((SheeplandAction) arg).getMove();
		System.out.println("\nERA STATA RICEVUTA LA MOSSA:" + mossa);
		listaWriter = mcc.getListaBufferedWriter();
		for (int i = 0; i < listaWriter.size(); i++) {
			bwl = listaWriter.get(i);
			writeAction(bwl, mossa);
		} // chiude il for
	} // chiude multicastWrite();

	public void multicastWritePlayersReady() throws IOException {
		listaWriter = mcc.getListaBufferedWriter();
		for (int i = 0; i < listaWriter.size(); i++) {
			bwl = listaWriter.get(i);
			for (int j = 0; j < 5; j++) {
				bwl.write(PlayersReady[j]);
				bwl.newLine();
				bwl.flush();
			}// chiude il for
		} // chiude il for
	} // chiude multicastWrite();

	public void multicastWriteString(String comando) throws IOException {
		System.out.println("\n stringa comando:" + comando);
		listaWriter = mcc.getListaBufferedWriter();
		for (int i = 0; i < listaWriter.size(); i++) {
			System.out.println("dentro il for di multicastWriteString");
			bwl = listaWriter.get(i);
			System.out.println("gettato bwl.");
			writeString(bwl, comando);
		} // chiude il for
	} // chiude multicastWriteString();

	private void writeAction(BufferedWriter bwl, Move mossa) throws IOException {
		String stringaMossa = mossa.toString();
		System.out.println("\nmando stringa mossa:" + stringaMossa);
		if (stringaMossa.equals("BUYCARD")
				|| stringaMossa.equals("MOVESHEPHERD")) {
			bwl.write(stringaMossa);
			bwl.newLine();
			bwl.flush();
			bwl.write(dato3);// cardtype o id road
			bwl.newLine();
			bwl.flush();
			System.out.println("\nInviati i dati:" + ID + dato2 + dato3);
		}
		if (stringaMossa.equals("MOVEANIMAL")) {
			bwl.write(stringaMossa);
			bwl.newLine();
			bwl.flush();
			bwl.write(dato3);// animal
			bwl.newLine();
			bwl.flush();
			bwl.write(dato4);// id region
			bwl.newLine();
			bwl.flush();
			System.out.println("\nInviati i dati:" + dato2 + dato3 + dato4);

		} else {
			throw new IllegalArgumentException();
		}

	}

	private void writeString(BufferedWriter bwl, String comando)
			throws IOException {
		switch (comando) {
		case "addplayer":
			bwl.write(comando);
			bwl.newLine();
			bwl.flush();
			bwl.write(Integer.toString(ID));// id player
			bwl.newLine();
			bwl.flush();
			bwl.write(dato2);// nickname
			bwl.newLine();
			bwl.flush();
			bwl.write(dato3);// gold
			bwl.newLine();
			bwl.flush();
			bwl.write(dato4);// card type
			bwl.newLine();
			bwl.flush();
			bwl.write(dato5);// card price
			bwl.newLine();
			bwl.flush();
			break;
		}
	}

	@Override
	public void showMessage(String message) {
		try {
			bw.write(NetAction.Command.MESSAGE.toString());
			bw.write(message);

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void nextPlayer(String currentPlayerID, String nextPlayer) {
		try {
			bw.write(NetAction.Command.NEXTPLAYER.toString());
			bw.newLine();
			bw.flush();
			bw.write(currentPlayerID);
			bw.newLine();
			bw.flush();
			bw.write(nextPlayer);
			bw.newLine();
			bw.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void setJRoadsName(String name) {
		try {
			bw.write(NetAction.Command.JROADNAME.toString());
			bw.newLine();
			bw.flush();
			bw.write(name);
			bw.newLine();
			bw.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void addListenerCardsAnimals() {
		try {

			bw.write(NetAction.Command.CARDANIMALS.toString());
			bw.newLine();
			bw.flush();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void showGoldCounters(String playerGold) {

		try {
			bw.write(NetAction.Command.SHOWGOLD.toString());
			bw.newLine();
			bw.flush();
			bw.write(playerGold);
			bw.newLine();
			bw.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void initializeJAnimals(ArrayList<Integer> allXregion,
			ArrayList<Integer> allYregion, ArrayList<String> allIDsregion) {
		try {
			bw.write(NetAction.Command.INITANIMALS.toString());
			bw.newLine();
			bw.flush();
			for (Integer x : allXregion) {
				bw.write(x.toString());
				bw.newLine();
				bw.flush();
			}
			for (Integer y : allYregion) {
				bw.write(y.toString());
				bw.newLine();
				bw.flush();
			}
			for (String id : allIDsregion) {
				bw.write(id);
				bw.newLine();
				bw.flush();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void initializeJRoads(ArrayList<Integer> allX,
			ArrayList<Integer> allY, ArrayList<String> allIDs) {

		try {
			bw.write(NetAction.Command.INITROADS.toString());
			bw.newLine();
			bw.flush();
			for (Integer x : allX) {
				bw.write(x.toString());
				bw.newLine();
				bw.flush();
			}
			for (Integer y : allY) {
				bw.write(y.toString());
				bw.newLine();
				bw.flush();
			}
			for (String id : allIDs) {
				bw.write(id);
				bw.newLine();
				bw.flush();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/** METODI GET */
	public BufferedWriter getBufferedWriter() {
		return bw;
	}


	public BufferedWriter getBufferedReader() {
		return bw;
	}


	public Socket getSocket() {
		return connection1;
	}

	public ServerSocket getServerSocket() {
		return serverSocket1;
	}

	public void setController(Controller controller) {
		this.controller = controller;
	}


	public int getID() {
		return ID;
	}
}// chiude la classe
