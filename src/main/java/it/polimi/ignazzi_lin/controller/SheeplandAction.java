package it.polimi.ignazzi_lin.controller;

import java.util.ArrayList;


public class SheeplandAction {

    public enum Move {    
 	MOVEANIMAL, MOVESHEPERD, BUYCARD, ENDGAME, SPENDGOLD;
 	
 	public static Move parseInput(String input) {
 		return Enum.valueOf(Move.class, input.toUpperCase());
 	}
     }
    
    private Move move;
    
    private ArrayList<String> args = new ArrayList<String>();
    
    public SheeplandAction(Move move, String... args) {
	this.move = move;
	for(int i = 0; i < args.length; i++) {
	    this.args.add(args[i]);
	}
    }
        
    public Move getMove() {
        return this.move;
    }
    
    public ArrayList<String> getArgs() {
	return this.args;
    }
    
}
