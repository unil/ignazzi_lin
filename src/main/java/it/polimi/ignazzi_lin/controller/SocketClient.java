package it.polimi.ignazzi_lin.controller;


import it.polimi.ignazzi_lin.controller.SheeplandAction.Move;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.util.Observable;


/**
 * La classe SocketClient effettua delle richieste al server scelto. data la natura di TEST di questo
 * codice, il computer locale svolgerà sia ruolo di client sia ruolo di server.
 * @IMPORTANTE l'istanza di SocketClient dovrà chiamare addObserver sull'istanza della classe Controller,
 * in modo da far funzionare il metodo recieveUpdate();
 * @author IgnazziMarco
 */

public class SocketClient extends Observable {

    private static String TimeStamp;
    private static Socket connection;
    public BufferedWriter bw;
    public BufferedReader br;
	private String readingString;
	private String myID;
	private String[] playersReady; 
	private String[] argAddPlayer;
	private String[] IDNickname = new String[11];
    
	/** COSTRUTTORE  */
	public SocketClient(String ip , int portNumber) {
		try {
			  
	      /* ottiene un oggetto indirizzo riferito al server, 
	       * stabilisce una connessione tramite socket.  
	       */
			
		  /**INIZIALIZZAZIONE DEL SOCKET*/
		  /**------------------------------------------------------------------------------------*/ 
		
	      connection = new Socket(ip, portNumber);
	      System.out.println(" Il SocketClient è inizializzato");
	      /**------------------------------------------------------------------------------------*/
	      
	      /*  Istanzio un oggetto BufferedOutputStream  ed un oggetto OutputStreamWriter
	       *  con un encoding (opzionale) potevamo usare il metodo BufferedOutputStream.write() 
	       *  per scrivere i bytes attraverso il socket ma in questo modo controlliamo anche l'encoding.
	       *  Inoltre l'uso del buffered writer aiuta,sia per la presenza del buffer
	       *  sia grazie al metodo newline(); che rende più comoda l'aggiunta del carattere terminatore: (char)13 a.k.a.\n 
	       *  Stessa cosa vale per gli inputStream e reader.
		   */
	      
	      /**INIZIALIZZAZIONE DEGLI STREAM */
	      /**------------------------------------------------------------------------------------*/
	      BufferedInputStream bis = new BufferedInputStream(connection.getInputStream());
	      InputStreamReader isr = new InputStreamReader(bis, "US-ASCII");
	      br = new BufferedReader(isr);	      
	      /**------------------------------------------------------------------------------------*/
	      BufferedOutputStream bos = new BufferedOutputStream(connection.getOutputStream());
	      OutputStreamWriter osw = new OutputStreamWriter(bos, "US-ASCII");
	      bw = new BufferedWriter(osw);
	      /**------------------------------------------------------------------------------------*/
	   
	      /**INIZIALIZZAZIONE DEGLI ARRAY DI STRINGHE*/
	      playersReady = new String[6];
	      argAddPlayer = new String[6]; 
	      
	      /**TEST CONNESIONE */
	      /**------------------------------------------------------------------------------------*/
	      TimeStamp = new java.util.Date().toString();
	      String contact = "Contattato il Socket Server all'host: "+ ip + " sulla porta numero: " + portNumber + " in data: " + TimeStamp;
	      bw.write(contact);
	      bw.newLine();
	      bw.flush();
	      System.out.println("contattato il server, in attesa di una risposta..\n");
	      readingFromServer(); // invocazione del metodo di ricezione 1 volta per completare l'init. (aknowledge)
	      System.out.println("CONNESSIONE STABILITA CORRETTAMENTE.\n");
	      myID=readingFromServer();
	      System.out.println("sono il giocatore numero:"+myID+"\n");

	      /**------------------------------------------------------------------------------------*/
		}
		catch (IOException f) {
			System.out.println("IOException: " + f);
			}
		catch (Exception g) {
				System.out.println("Exception: " + g);
		}
	}// chiude il costruttore.
	
	/**
	 * @return
	 */
	public SheeplandAction recieveMoves(){
		try {

		SheeplandAction movesRecieved;
		String readingString0;
			readingString0 = readingFromServer();

		String readingString1 = readingFromServer();
		if(readingString0.equals(Move.MOVEANIMAL.toString() )){
			String readingString2 = readingFromServer();
			movesRecieved = new SheeplandAction(Move.parseInput(readingString0),readingString1,readingString2);
			return movesRecieved;
		}
		else{
			movesRecieved = new SheeplandAction(Move.parseInput(readingString0),readingString1);
			return movesRecieved;
		}
		
		
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
		
	}
	
	
	public String[] recieveUpdateCommand() throws IOException{ //TODO ora questo andrà chiamato in modo diverso
			System.out.println("IN ATTESA DI LEGGERE nel RECIEVEUPDATECOMMAND");

			readingString = readingFromServer();
			if(readingString != null){
			//	writingToClient(bw,br, readingString);
			
			switch (readingString){
				case"addplayer":
					argAddPlayer[0]="addplayer"; 
					argAddPlayer[1]=readingFromServer();// id player
					argAddPlayer[2]=readingFromServer();// nickname
					argAddPlayer[3]=readingFromServer();// gold
					argAddPlayer[4]=readingFromServer();// type
					argAddPlayer[5]=readingFromServer();// price
					return argAddPlayer;
				
				case "stringReady":
					System.out.println("riconosciuta la stringa stringReady,");

					playersReady[0]="stringReady";
					for(int i=1;i<6;i++){
						playersReady[i]=readingFromServer();
					}
					return playersReady;
				default: 
					System.out.println("Grave errore: arrivata al client un comando illecito");
					throw new IllegalArgumentException();
					
				} // chiude switch
			} // chiude if
			return null;
	} // chiude recieveUpdate();
					

	public String[] recievePlayersIDNickname() {

		try {
			System.out.println("dentro recievePlayersIDNickname");
			String command = readingFromServer();
			String size;
			size=readingFromServer();
			String id = readingFromServer();
			int sizeInt=Integer.parseInt(size);
			IDNickname= new String[sizeInt];
			IDNickname[0]=command;
			IDNickname[1]=size;
			IDNickname[2]=id;
 			for(int i=3;i<sizeInt;i++){
				IDNickname[i]=readingFromServer();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return IDNickname;

	}
	
	
	public SheeplandAction recieveUpdate() throws IOException{
		String dato1,dato2,dato3,dato4 = null;
		
			readingString = readingFromServer();	//lettura mossa
			dato1=myID;
			if(readingString != null){ 
			//	writingToClient(bw,br, readingString);
			
			switch (readingString.toLowerCase()){
				case "buycard":
					dato1=readingFromServer();// id player va tolto
					dato2=readingFromServer();// id shepherd
					dato3=readingFromServer();// card type
					SheeplandAction argCard = new SheeplandAction(SheeplandAction.Move.BUYCARD,dato1,dato2,dato3);
					return argCard;
					
				case "movesheperd":
					dato1=readingFromServer(); // id player
					dato2=readingFromServer(); // id shepherd
					dato3=readingFromServer(); // id road
					SheeplandAction argPlayer = new SheeplandAction(SheeplandAction.Move.MOVESHEPERD,dato1,dato2,dato3);
					return argPlayer;
					
				case "moveanimal": 
					dato1=readingFromServer(); // id player
					dato2=readingFromServer(); // id shepherd
					dato3=readingFromServer(); // animal
					dato4=readingFromServer(); // id region
					SheeplandAction argAnimal = new SheeplandAction(SheeplandAction.Move.MOVEANIMAL,dato1,dato2,dato3, dato4); 
					return argAnimal;
					
				default: 
					System.out.println("Grave errore: arrivata al client una mossa illecita");
					break;
				} // chiude switch
			} // chiude if
			return null;
	} // chiude recieveUpdate();
	
	
	
      /**METODO DI LETTURA DAL SERVER*/
	  public String readingFromServer() throws IOException{
		  String received = null;
		  
		  do{
		  }while(!br.ready());
		  received = br.readLine();
		  System.out.println("Ho ricevuto dal server:"+ received);
    	  return received;
	  }// chiude readingFromServer();
	  
	  /**METODO PER L'INVIO Di ENUM (passati come parametro.) */
	  public void writeMossa(SheeplandAction.Move mossa){
	    try{
		  	String stringaMossa = mossa.toString();
	    	bw.write(stringaMossa);
	    	bw.newLine();
	    	bw.flush();
	    	System.out.println("inviato il tipo di mossa"); 
	  	}
		catch (Exception g) {
				g.printStackTrace();
		}
	  }	 
	  
	  /**METODO PER L'INVIO Di ENUM (passati come parametro.) */
	  public void writeNetAction(NetAction.Command mossa){
	    try{
		  	String stringaNetCommand = mossa.toString();
	    	bw.write(stringaNetCommand);
	    	bw.newLine();
	    	bw.flush();
	    	System.out.println("inviato il tipo di mossa"); 
	  	}
	    catch (Exception g) {
			g.printStackTrace();
	    }
	  }	 
	  
	  // utile nel caso di comandi del tipo addplayer/Update.
	  /**METODO PER L'INVIO Di COMANDO (passati come parametro.) */
	  public void writeString(String stringa){
		    try{
		    	bw.write(stringa);
		    	bw.newLine();
		    	bw.flush();
		    	System.out.println("inviato dal client al server la stringa di comando:"+stringa+"\n"); 
		  	}
			catch (IOException f) {
				System.out.println("IOException: " + f);
				}
			catch (Exception g) {
					System.out.println("Exception: " + g);
			}
		  }
	        		 
	
	/**METODO CHE INVIA AL SERVER I DATI INERENTI IL MOVIMENTO DEL PASTORE*/
	public void moveSheperdNet(String idPlayer, String idSheperd, String idRoad) {
	  try{
		  bw.write(idPlayer);
		  bw.newLine();
		  bw.flush();
		  bw.write(idSheperd);
		  bw.newLine();
		  bw.flush();
		  bw.write(idRoad);
		  bw.newLine();
		  bw.flush();
		}
		catch (IOException f) {
			System.out.println("IOException: " + f);
			}
		catch (Exception g) {
				System.out.println("Exception: " + g);
		}	
	}
	
	/**METODO CHE INVIA AL SERVER I DATI INERENTI IL MOVIMENTO DEL PASTORE*/
	
	public void moveSheperdNet(String idRoad) {
	  try{
		  bw.write(idRoad);
		  bw.newLine();
		  bw.flush();
		}
		catch (IOException f) {
			System.out.println("IOException: " + f);
			}
		catch (Exception g) {
				System.out.println("Exception: " + g);
		}	
	}
	

	/**METODO CHE INVIA AL SERVER I DATI INERENTI IL MOVIMENTO DELLE PECORE*/
	public void moveAnimalNet(String idPlayer, String idSheperd, String animal,String idOriginRegion, String idDestinationRegion) {
	  try{
		  bw.write(idPlayer);
		  bw.newLine();
		  bw.flush();
		  bw.write(idSheperd);
		  bw.newLine();
		  bw.flush();
		  bw.write(animal);
		  bw.newLine();
		  bw.flush();
		  bw.write(idOriginRegion);
		  bw.newLine();
		  bw.flush();
		  bw.write(idDestinationRegion);
		  bw.newLine();
		  bw.flush();
		}
		catch (IOException f) {
			System.out.println("IOException: " + f);
			}
		catch (Exception g) {
				System.out.println("Exception: " + g);
		}
	}
	

	/**METODO CHE INVIA AL SERVER I DATI INERENTI IL MOVIMENTO DELLE PECORE*/
	public void moveAnimalNet(String animal,String idOriginRegion) {
	  try{

		  bw.write(animal);
		  bw.newLine();
		  bw.flush();
		  bw.write(idOriginRegion);
		  bw.newLine();
		  bw.flush();
		}
		catch (IOException f) {
			System.out.println("IOException: " + f);
			}
		catch (Exception g) {
				System.out.println("Exception: " + g);
		}
	}
	
	/**METODO CHE INVIA AL SERVER I DATI INERENTI L'ACQUISTO DELLE TESSERE TERRENO*/
	public void buyCardNet(String idPlayer, String idSheperd,String cardType,String cardPrice,String playersCardCount) {
	  try{
		  bw.write(idPlayer);
		  bw.newLine();
		  bw.flush();
		  bw.write(idSheperd);
		  bw.newLine();
		  bw.flush();
		  bw.write(cardType);
		  bw.newLine();
		  bw.flush();

		  bw.write(cardPrice);
		  bw.newLine();
		  bw.flush();

		  bw.write(playersCardCount);
		  bw.newLine();
		  bw.flush();
		}
		catch (IOException f) {
			System.out.println("IOException: " + f);
			}
		catch (Exception g) {
				System.out.println("Exception: " + g);
		}
	}
	

	/**METODO CHE INVIA AL SERVER I DATI INERENTI L'ACQUISTO DELLE TESSERE TERRENO*/
	public void buyCardNet(String cardType) {
	  try{
		  bw.write(cardType);
		  bw.newLine();
		  bw.flush();
		}
		catch (IOException f) {
			System.out.println("IOException: " + f);
			}
		catch (Exception g) {
				System.out.println("Exception: " + g);
		}
	}
	
	/**METODO per l'invio dei dati inerenti il posizionamento iniziale dei pastori*/
	public void setShepherdPositionNet(String idPlayer,String idShepherd, String idRoad,String color) {
		  try{
			  bw.write(idPlayer);
			  bw.newLine();
			  bw.flush();
			  bw.write(idShepherd);
			  bw.newLine();
			  bw.flush();
			  bw.write(idRoad);
			  bw.newLine();
			  bw.flush();
			  bw.write(color);
			  bw.newLine();
			  bw.flush();
			}
			catch (IOException f) {
				System.out.println("IOException: " + f);
				}
			
		}
	

	/**METODO CHE INVIA AL SERVER I DATI INERENTI L'AGGIUNTA GIOCATORE*/
	public void addPlayerNet(String id,String nickname){
		
		  try{
			  bw.write(id);
			  bw.newLine();
			  bw.flush();
			  bw.write(nickname);
			  bw.newLine();
			  bw.flush();
			}
			catch (IOException f) {
				System.out.println("IOException: " + f);
				}
	}
		

	
    

	/**METODI GET*/
    public BufferedWriter getBufferedWriter(){
    	return bw;
    }
    public BufferedWriter getBufferedReader(){
    	return bw;
    }
    public Socket getSocket(){
    	return connection;
    }
    public int getIDclient(){
    	int id= Integer.parseInt(myID);
    	return id;
    }
    public String[] getStringReadyUpdated(){
    	return playersReady;
    }




}// chiude la classe
