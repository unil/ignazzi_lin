package it.polimi.ignazzi_lin.controller;

import it.polimi.ignazzi_lin.model.Constants;
import it.polimi.ignazzi_lin.model.ModelNet;
import it.polimi.ignazzi_lin.model.Region;
import it.polimi.ignazzi_lin.model.Road;
import it.polimi.ignazzi_lin.model.TerrainCard;
import it.polimi.ignazzi_lin.model.TerrainType;
import it.polimi.ignazzi_lin.model.XmlParser;
import it.polimi.ignazzi_lin.view.View;

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

public class ControllerClient implements Observer {

    private ModelNet model;

    private View view;

    private String myID = Integer.toString(0);

    private String currentShepherdID = Integer.toString(0);

    private XmlParser xmlParser;

    private int playersNumber;

    public ControllerClient(ModelNet model, View view) {
	this.model = model;
	this.view = view;
	view.addObserver(this);
	model.addObserver(view);
	myID = model.getMyID();
    }

    @Override
    public void update(Observable o, Object arg) {
	if (o != view
		|| (!(arg instanceof SheeplandAction)
			&& !(arg instanceof ViewAction)
			&& !(arg instanceof String)
			&& !(arg instanceof String[]) && !(arg instanceof BufferedWriter))) {
	    throw new IllegalArgumentException();
	}
	if (arg instanceof BufferedWriter) {
	    System.out
		    .println("arrivato al controller un BufferedWriter, -> disinteresse.");
	} else {
	    if (arg instanceof SheeplandAction) {
		sendAction((SheeplandAction) arg);
	    }
	    if (arg instanceof ViewAction) {
		viewActionReceived((ViewAction) arg);
	    }
	    if (arg instanceof String) {
		initModel((String) arg);
	    }
	    if (arg instanceof String[]) {
		String[] argcasted = (String[]) arg;

		switch (argcasted[0]) {

		case "broadcastStart":
		    System.out
			    .println("dentro controller: next move: model.broadcastStart");

		    model.broadcastStart(argcasted[1]);
		    break;

		case "stringReady":
		    for (int i = 0; i < 6; i++) {
			if (argcasted[i].equals("v")) {
			    model.setReady(i);

			}
		    }
		    break;
		case "posizione":
		    model.setReady(Integer.parseInt(argcasted[1]));
		    System.out.println("giocatore "
			    + (Integer.parseInt(argcasted[1]) - 1) + " pronto");

		    break;

		case "updateRequestPlayersReady": // dai client
		    model.updatePlayersReady(argcasted[1]);
		    break;
		case "updateRequestPlayers":
		    model.updatePlayers(argcasted[1]);
		    break;

		case "addplayerOnline":
		    System.out
			    .println("case addplayerOnline, modifica model server: ");
		    String id = argcasted[1];

		    String nicknamePlayer = argcasted[2];
		    int gold = Integer.parseInt(argcasted[3]);
		    TerrainType type = TerrainType.parseInput(argcasted[4]);
		    int price = Integer.parseInt(argcasted[5]);
		    TerrainCard card = new TerrainCard(type, price);
		    model.addPlayer(id, nicknamePlayer, gold, card);
		    System.out
			    .println("case addplayerOnline FINITO, modifica model server: \n");

		    break;
		case "addplayer":
		    System.out
			    .println("case addplayer, invia i dati al server: ");
		    myID = model.getMyID();

		    String nickname = argcasted[2];
		    model.addPlayer(myID, nickname, Constants.PLAYER_GOLD,
			    new TerrainCard(model.getRandomTerrainType(), 0));
		    System.out
			    .println("case addplayerOnline FINITO, modifica model server: \n");
		    break;
		default:
		    throw new IllegalArgumentException();
		}

	    }// chiude if instance of String[]
	}// chiude l'else
    }

    private void viewActionReceived(ViewAction arg) {
	ViewAction.Command viewActionCommand = arg.getCommand();
	switch (viewActionCommand) {
	case START:
	    model.startAction();
	    playersNumber = model.getPlayersSizeFromServer();

	    view.showGoldCounters(Integer.toString(Constants.PLAYER_GOLD));
	    if (playersNumber == 2) {
		for (int id = 0; id < playersNumber; id++) {
		    model.addSheperdToPlayer(Integer.toString(id),
			    Integer.toString(1), Constants.TWO_PP_GOLD);
		}
		view.showGoldCounters(Integer.toString(Constants.TWO_PP_GOLD));
	    }

	    ArrayList<Road> roads = xmlParser.getRoads();
	    ArrayList<Integer> allXroad = new ArrayList<Integer>();
	    ArrayList<Integer> allYroad = new ArrayList<Integer>();
	    ArrayList<String> allIDsroad = new ArrayList<String>();
	    for (Road road : roads) {
		allXroad.add(road.getX());
		allYroad.add(road.getY());
		allIDsroad.add(road.getId());
	    }
	    view.initializeJRoads(allXroad, allYroad, allIDsroad);

	    ArrayList<Region> regions = xmlParser.getRegions();
	    ArrayList<Integer> allXregion = new ArrayList<Integer>();
	    ArrayList<Integer> allYregion = new ArrayList<Integer>();
	    ArrayList<String> allIDsregion = new ArrayList<String>();
	    for (Region region : regions) {
		allXregion.add(region.getX());
		allYregion.add(region.getY());
		allIDsregion.add(region.getId());
	    }
	    view.initializeJAnimals(allXregion, allYregion, allIDsregion);

	    int playerCount = Integer.parseInt(myID) + 1;
	    int shepherdCount = Integer.parseInt(currentShepherdID) + 1;

	    showInitialCard();

	    view.showMessage("Giocatore n " + playerCount
		    + "° scegli la posizione del pastore n " + shepherdCount
		    + "°.");
	    view.nextPlayer(myID, myID);

	    break;
	default:
	    throw new IllegalArgumentException();
	}
    }

    private void sendAction(SheeplandAction arg) {
	SheeplandAction.Move move = arg.getMove();
	ArrayList<String> args = arg.getArgs();
	switch (move) {

	case MOVESHEPERD:
	    String idRoad = args.get(0);
	    model.moveSheperd(idRoad);
	    break;

	case BUYCARD:
	    String cardType = args.get(0);
	    model.buyCard(cardType);
	    break;

	case MOVEANIMAL:
	    String animal = args.get(0);
	    String idOriginRegion = args.get(1);
	    model.moveAnimal(animal, idOriginRegion);
	    break;

	default:
	    throw new IllegalArgumentException();
	}
    }

    private void showInitialCard() {

	TerrainCard card = model.getPlayer(myID).getCards().get(0);
	String cardType = card.getType().toString();
	String cardPrice = Integer.toString(card.getPrice());
	model.buyCard(myID, currentShepherdID, cardType, cardPrice, "1");
	showCards();
	view.showMessage("Hai ricevuto una tessera di tipo: " + cardType);
    }

    private void showCards() {
	for (TerrainType type : TerrainType.values()) {
	    if (type != TerrainType.SHEEPSBURG) {
		int playersCardCount = 0;
		ArrayList<TerrainCard> cardsOfType = model.getDeckList().get(
			type);
		String cardPrice = Integer.toString((cardsOfType
			.get(cardsOfType.size() - 1).getPrice()));
		for (TerrainCard card : model.getPlayer(myID).getCards()) {
		    if (card.getType().equals(type))
			playersCardCount++;
		}
		model.buyCard(myID, currentShepherdID, type.toString(),
			cardPrice, Integer.toString(playersCardCount));
	    }
	}
    }

    private void initModel(String arg) {
	switch (arg) {
	case "waiting":
	    model.broadcastReading();
	    break;
	case "broadcastStart":
	    model.broadcastStart();
	    break;
	case "updateRequestPlayersReady": // dai client
	    model.updatePlayersReady();
	    System.out.println("fine model.updatePlayersReady");
	    break;
	case "updateRequestPlayers": // dai client
	    model.updatePlayers();
	    System.out.println("fine model.updatePlayers");
	    break;
	default:
	    throw new IllegalArgumentException();
	}
    }

}
