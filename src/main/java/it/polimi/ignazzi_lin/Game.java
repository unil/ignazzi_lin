package it.polimi.ignazzi_lin;

import it.polimi.ignazzi_lin.controller.Controller;
import it.polimi.ignazzi_lin.model.Model;
import it.polimi.ignazzi_lin.model.XmlParser;
import it.polimi.ignazzi_lin.view.GUI;
import it.polimi.ignazzi_lin.view.View;

public class Game {
    
    private static final int DICEFACES = 6;
    
    private static final int DECKDIMENSION = 4;
    
    private Model model;
    
    private View view;
    
    private Controller controller;
    
    private XmlParser xmlParser;
     
    public Game() {
	this.xmlParser = new XmlParser();
	this.xmlParser.loadMap();
	model = new Model(DECKDIMENSION, DICEFACES, xmlParser.getRoads(), xmlParser.getRegions(), xmlParser.getTerrainTypes());
	view = new GUI();		
	controller = new Controller(model, view);
	view.addObserver(controller);
	model.addObserver(view);
    }
	
    public static void main(String[] args) {
	Game main = new Game();
	main.run();

    }
    
    private void run() {
	this.view.run();
    }
    
}