package it.polimi.ignazzi_lin.model;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class ModelTest {
    
    XmlParser xmlParser = new XmlParser(); 
    Model modelTest = new Model(5, 6, xmlParser.getRoads(), xmlParser.getRegions(), xmlParser.getTerrainTypes());

    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
	System.out.println("Inizio suite di test: "
		+ System.currentTimeMillis());
    }

    @AfterClass
    public static void setUpAfterClass() throws Exception {
	System.out.println("Fine suite di test: " + System.currentTimeMillis());
    }

    @Before
    public void setUp() throws Exception {
	System.out.println("Inizializzazione test.");
    }

    @After
    public void tearDown() {
	System.out.println("Fine test.");
    }
    
    @Test
    public void addShepherdTest() {
	modelTest.addPlayer("0", "name", 12, new TerrainCard(TerrainType.FOREST, 1));
	Player playerTest = modelTest.getPlayer("0");
	modelTest.addSheperdToPlayer("0", "0", 37);
	assertEquals(37, playerTest.getGold());
	assertEquals(2, playerTest.getShepherds().size());
    }
    
    @Test
    public void createDecksTest() {
	modelTest.createDecks(6);
	assertEquals(null, modelTest.getDeckList().get(TerrainType.SHEEPSBURG));
	assertEquals(0, modelTest.getDeckList().get(TerrainType.FIELD).get(6).getPrice());
    }
    
}
