package it.polimi.ignazzi_lin.model;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class ShepherdTest {

    Shepherd shepherdTest = new Shepherd(new Player("0", "tsk", 20,
	    new TerrainCard(TerrainType.FIELD, 2)), "1", 30);

    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
	System.out.println("Inizio suite di test: "
		+ System.currentTimeMillis());
    }

    @AfterClass
    public static void setUpAfterClass() throws Exception {
	System.out.println("Fine suite di test: " + System.currentTimeMillis());
    }

    @Before
    public void setUp() throws Exception {
	System.out.println("Inizializzazione test.");
    }

    @After
    public void tearDown() {
	System.out.println("Fine test.");
    }

    @Test
    public void playerGoldEqualsShepherdsTest() {
	assertEquals(30, shepherdTest.getPlayerOwner().getGold());
    }

    @Test
    public void setPositionTest() {
	shepherdTest.setPosition(new Road("1", 4, "1x1"));
	assertEquals(true, shepherdTest.getPosition().isOccupied());
    }

    @Test
    public void moveSheepTest() {
	AnimalType animalType = AnimalType.BLACKSHEEP;
	Region originRegion = new Region("1", TerrainType.FOREST,
		AnimalType.BLACKSHEEP, 2, "9x3");
	Region destinationRegion = new Region("2", TerrainType.SHEEPSBURG,
		AnimalType.BLACKSHEEP, 1, "4x3");
	shepherdTest.moveSheep(animalType, originRegion, destinationRegion);
	assertEquals(1,
		(int) originRegion.getAnimals().get(AnimalType.BLACKSHEEP));
    }

}
