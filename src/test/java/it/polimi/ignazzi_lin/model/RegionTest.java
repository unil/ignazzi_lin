package it.polimi.ignazzi_lin.model;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class RegionTest {

    Region regionTest = new Region("1", TerrainType.MOUNTAIN, AnimalType.SHEEP,
	    3, "0x");

    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
	System.out.println("Inizio suite di test: "
		+ System.currentTimeMillis());
    }

    @AfterClass
    public static void setUpAfterClass() throws Exception {
	System.out.println("Fine suite di test: " + System.currentTimeMillis());
    }

    @Before
    public void setUp() throws Exception {
	System.out.println("Inizializzazione test.");
    }

    @After
    public void tearDown() {
	System.out.println("Fine test.");
    }

    @Test
    public void getOppositeTest() {
	Road roadTest = new Road("2", 3, "6x1");
	ArrayList<Region> regions = new ArrayList<Region>();
	Region regionNew = new Region("7", TerrainType.DESERT,
		AnimalType.BLACKSHEEP, 1, "3x4");
	regions.add(regionTest);
	regions.add(regionNew);
	roadTest.setNearRegions(regions);
	assertEquals(regionNew, regionTest.getOppositeRegion(roadTest));
    }

    @Test
    public void containsAnimalTest() {
	assertEquals(false, regionTest.contains(AnimalType.WOLF));
    }
    
    @Test
    public void moveAnimalTest(){
	Region destinationRegion = new Region("2", TerrainType.VALLEY, AnimalType.SHEEP, 2, "42x42");
	regionTest.moveAnimal(AnimalType.SHEEP, destinationRegion);
	assertEquals(3, (int)destinationRegion.getAnimals().get(AnimalType.SHEEP));
    }
    
}
